import { NgModule } from '@angular/core';
import { ProfileComponent } from './profile.component';
import { SharedModule } from '@shared';
import { RouterModule } from '@angular/router';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { UpdateAvatarComponent } from './update-avatar/update-avatar.component';

@NgModule({
    declarations: [ProfileComponent, UpdateAvatarComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: '',
                component: ProfileComponent
            }
        ]),
        NzCardModule,
        NzTabsModule,
        NzDividerModule,
        NzSpinModule,
        NzAvatarModule,
        NzIconModule
    ]
})
export class ProfileModule {}
