import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-update-avatar',
    templateUrl: './update-avatar.component.html',
    styleUrls: ['./update-avatar.component.less']
})
export class UpdateAvatarComponent {
    @Input()
    avatar!: string;
}
