import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { map, Observable } from 'rxjs';
import { UserBasicService } from '@pages/user/_base';
import { UserForm, UserInfoVO } from '@types';

@Injectable({
    providedIn: 'root'
})
export class UserProfileService extends UserBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    /**
     * 查询用户个人信息
     * @param params
     */
    getUserProfile(): Observable<ApiResToComponent<UserInfoVO>> {
        return this.client.get(this.apiUrl.user.profile).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询用户个人信息', color: 'grey' }, true, false);
            })
        );
    }
    /**
     * 修改用户个人信息
     * @param params
     */
    updateUserProfile(params: UserForm): Observable<ApiResToComponent<UserInfoVO>> {
        return this.client.put(this.apiUrl.user.profile, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '修改用户个人信息', color: 'grey' }, true, true);
            })
        );
    }
    /**
     * 用户密码重置
     * @param params
     */
    updateUserPwd(params: { oldPassword: string; newPassword: string }): Observable<ApiResToComponent<UserInfoVO>> {
        return this.client.put(this.apiUrl.user.updatePwd, {}, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '用户密码重置', color: 'grey' }, true, true);
            })
        );
    }
}
