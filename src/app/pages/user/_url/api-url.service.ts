import { Injectable } from '@angular/core';
import { AbstractApiUrlDataService } from '@basic/abstracts';

@Injectable({ providedIn: 'any' })
export class ApiUrlDataOfUserService extends AbstractApiUrlDataService {
    /**
     * 优惠券批次管理
     */
    ticketBatch = {
        // 优惠券生成批次-分页列表查询
        list: `${this.baseUrl}/ticketBatch/list`,
        // 优惠券生成批次-新增
        add: `${this.baseUrl}/ticketBatch/add`,
        // 优惠券生成批次-更新
        update: `${this.baseUrl}/ticketBatch/update`,
        // 优惠券生成批次-删除
        delete: `${this.baseUrl}/ticketBatch/delete`,
        // 优惠券生成批次-获取单个
        getOne: `${this.baseUrl}/ticketBatch/getOne`
    };

    user = {
        profile: `${this.baseUrl}/system/user/profile`,
        updatePwd: `${this.baseUrl}/system/user/profile/updatePwd`
    };

    /**
     * 优惠券详细信息
     */
    ticketRecord = {
        // 优惠券详细信息-分页列表查询
        list: `${this.baseUrl}/ticketRecord/list`,
        // 优惠券详细信息-新增
        add: `${this.baseUrl}/ticketRecord/add`,
        // 优惠券详细信息-更新
        update: `${this.baseUrl}/ticketRecord/update`,
        // 优惠券详细信息-删除
        delete: `${this.baseUrl}/ticketRecord/delete`,
        // 优惠券详细信息-获取单个
        getOne: `${this.baseUrl}/ticketRecord/getOne`
    };
}
