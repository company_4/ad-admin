import { Injectable, Injector } from '@angular/core';
import { CommBasicService } from '@basic';

import { ApiUrlDataOfUserService } from '../_url';

@Injectable()
export class UserBasicService extends CommBasicService {
    apiUrl: ApiUrlDataOfUserService;

    constructor(protected override injector: Injector) {
        super(injector);
        this.apiUrl = this.injector.get(ApiUrlDataOfUserService);
    }
}
