import { Directive, Injector } from '@angular/core';
import { BaseOfSimpleForm } from '@basic';
import { NzModalRef } from 'ng-zorro-antd/modal';

import { ApiUrlDataOfUserService } from '../_url';
/**
 * 基础表单控件
 */
@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class BaseOfUserForm<R, P> extends BaseOfSimpleForm<R, P> {
    apiUrl: ApiUrlDataOfUserService;

    constructor(public override injector: Injector) {
        super(injector);
        this.apiUrl = this.injector.get(ApiUrlDataOfUserService);
    }
}
