import { Directive, Injector } from '@angular/core';
import { BaseOfSimpleTable } from '@basic';

import { ApiUrlDataOfUserService } from '../_url';

/**
 * 根据url请求所有数据后再赋值到表格
 */
@Directive()
export abstract class BaseOfUserTable extends BaseOfSimpleTable {
    apiUrl: ApiUrlDataOfUserService;

    constructor(public override injector: Injector) {
        super(injector);
        this.apiUrl = this.injector.get(ApiUrlDataOfUserService);
    }
}
