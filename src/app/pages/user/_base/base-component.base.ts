import { Directive, Injector } from '@angular/core';
import { BaseOfSimpleComponent } from '@basic';

import { ApiUrlDataOfUserService } from '../_url';

/**
 * 基础Component控件,主要处理一些基础通用的问题，比如，退出路由时，关闭modal
 */
@Directive()
export abstract class BaseOfUserComponent extends BaseOfSimpleComponent {
    apiUrl: ApiUrlDataOfUserService;

    constructor(protected override injector: Injector) {
        super(injector);
        this.apiUrl = this.injector.get(ApiUrlDataOfUserService);
    }
}
