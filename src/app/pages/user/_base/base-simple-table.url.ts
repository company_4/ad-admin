import { Directive, Injector } from '@angular/core';
import { UrlOfSimpleTable } from '@basic';
import { ApiUrlDataOfUserService } from '../_url';

/**
 * 根据url请求单页数据，后台翻页
 */
@Directive()
export abstract class UrlOfUserTable extends UrlOfSimpleTable {
    apiUrl: ApiUrlDataOfUserService;

    constructor(public override injector: Injector) {
        super(injector);
        this.apiUrl = this.injector.get(ApiUrlDataOfUserService);
    }
}
