import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';

import { UserLockComponent } from './lock.component';

const COMPONENTS = [UserLockComponent];

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NzFormModule, NzInputModule, NzAvatarModule, NzButtonModule],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class UserLockModule {}
