import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BasicPipesModule } from '../../pipes';
import { ShowJsonComponent } from './json.component';

const COMPONENTS = [ShowJsonComponent];

@NgModule({
  imports: [CommonModule, BasicPipesModule],
  declarations: [...COMPONENTS],
  exports: COMPONENTS
})
export class ShowJsonModule {}
