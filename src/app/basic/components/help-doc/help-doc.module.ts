import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NzIconModule } from 'ng-zorro-antd/icon';

import { HelpDocComponent } from './help-doc.component';

@NgModule({
  declarations: [HelpDocComponent],
  imports: [CommonModule, NzIconModule],
  exports: [HelpDocComponent]
})
export class HelpDocModule {}
