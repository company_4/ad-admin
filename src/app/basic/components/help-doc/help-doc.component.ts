import { Component, Input, OnInit } from '@angular/core';

import { AppConfigService } from '../../services';

@Component({
  selector: 'app-help-doc',
  templateUrl: './help-doc.component.html',
  styleUrls: ['./help-doc.component.less']
})
export class HelpDocComponent {
  @Input()
  header: boolean = false;

  constructor(public aConfig: AppConfigService) {}
}
