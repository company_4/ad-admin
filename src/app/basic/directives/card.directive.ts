import { AfterViewInit, Directive, ElementRef, Renderer2 } from '@angular/core';

/**
 * 样式类指令
 */
@Directive({
    selector: '[space-card]'
})
export class CustomCardDirective implements AfterViewInit {
    constructor(private er: ElementRef, private renderer2: Renderer2) {}

    ngAfterViewInit(): void {
        this.renderer2.addClass(this.er.nativeElement, 'space-card');
    }
}
