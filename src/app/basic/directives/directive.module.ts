import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CustomCardDirective } from './card.directive';
import { DebounceClickDirective } from './debounce-click.directive';
import { DebounceScrollDirective } from './debounce-scroll.directive';
import { ThrottleClickDirective } from './throttle-click.directive';
import { ThrottleScrollDirective } from './throttle-scroll.directive';

const DIRECTIVES = [CustomCardDirective, DebounceClickDirective, DebounceScrollDirective, ThrottleClickDirective, ThrottleScrollDirective];

@NgModule({
  imports: [CommonModule],
  providers: [],
  declarations: [...DIRECTIVES],
  exports: [...DIRECTIVES]
})
export class BasicDirectiveModule {}
