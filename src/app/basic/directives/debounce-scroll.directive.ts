import { Directive, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

/**
 * 操作类指令
 * 按钮点击延迟防抖，会延时执行
 */
@Directive({
  selector: '[debounce-scroll]'
})
export class DebounceScrollDirective implements OnInit, OnDestroy {
  @Input() debounceTime = 500;
  // eslint-disable-next-line @angular-eslint/no-output-on-prefix
  @Output() readonly onScroll = new EventEmitter();
  private scrolls = new Subject<any>();
  private scroll$: Subscription | any;

  constructor() {}

  ngOnInit(): void {
    this.scroll$ = this.scrolls.pipe(debounceTime(this.debounceTime)).subscribe(e => {
      this.onScroll.emit(e);
    });
  }

  ngOnDestroy(): void {
    this.scroll$.unsubscribe();
  }

  @HostListener('scroll', ['$event'])
  clickEvent(event: any): void {
    event.preventDefault();
    event.stopPropagation();
    this.scrolls.next(event);
  }

  // @HostBinding()
  // onclick() {
  //     //
  // }
}
