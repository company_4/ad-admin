import { Directive, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { throttleTime } from 'rxjs/operators';

/**
 * 操作类指令
 * 函数节流，会立即执行
 */
@Directive({
  selector: '[throttle-scroll]'
})
export class ThrottleScrollDirective implements OnInit, OnDestroy {
  @Input() throttleTime = 200;
  // eslint-disable-next-line @angular-eslint/no-output-on-prefix
  @Output() readonly onScroll = new EventEmitter();
  private scrolls = new Subject<any>();
  private scroll$!: Subscription;

  constructor() {}

  ngOnInit(): void {
    this.scroll$ = this.scrolls.pipe(throttleTime(this.throttleTime)).subscribe(e => {
      this.onScroll.emit(e);
    });
  }

  ngOnDestroy(): void {
    this.scroll$.unsubscribe();
  }

  @HostListener('scroll', ['$event'])
  clickEvent(event: any): void {
    event.preventDefault();
    event.stopPropagation();
    this.scrolls.next(event);
  }

  // @HostBinding()
  // onclick() {
  //     //
  // }
}
