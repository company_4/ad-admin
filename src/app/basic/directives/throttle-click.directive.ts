import { Directive, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { throttleTime } from 'rxjs/operators';

/**
 * 操作类指令
 * 按钮点击节流，会立即执行
 */
@Directive({
  selector: '[throttle-click]'
})
export class ThrottleClickDirective implements OnInit, OnDestroy {
  @Input() throttleTime = 500;
  @Output() readonly throttleClick = new EventEmitter();
  private clicks = new Subject<any>();
  private subscription: Subscription | any;

  constructor() {}

  ngOnInit(): void {
    this.subscription = this.clicks.pipe(throttleTime(this.throttleTime)).subscribe(e => {
      this.throttleClick.emit(e);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  @HostListener('click', ['$event'])
  clickEvent(event: any): void {
    event.preventDefault();
    event.stopPropagation();
    this.clicks.next(event);
  }

  // @HostBinding()
  // onclick() {
  //     //
  // }
}
