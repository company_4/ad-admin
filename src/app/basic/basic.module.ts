import { UserLockModule } from './components';
import { BasicDirectiveModule } from './directives';
import { BasicPipesModule } from './pipes';
import { BasicValidatorsModule } from './validators';

export const BASIC_MODULES = [BasicPipesModule, BasicDirectiveModule, UserLockModule, BasicValidatorsModule];
