// API接口返回数据格式
export interface ApiResponse {
    msg?: string;
    code?: number | string;
    data?: any;
}

export interface ApiTableResponse {
    msg?: any;
    code?: number | string;
    total: number;
    rows: any;
}

export interface ApiResToComponent<T> {
    isComplete: boolean;
    msg?: string;
    code?: number | string;
    data?: T;

    [key: string]: any;
}

// 返回成功的结果
export class SuccessApiRequest<T> {
    constructor(obj: ApiResponse) {
        this.apiReturn = {
            ...obj,
            ...{ isComplete: true }
        };
    }

    public apiReturn: ApiResToComponent<T>;
}

// 返回失败的结果
export class ErrorApiRequest<E> {
    constructor(obj: ApiResponse) {
        this.apiReturn = {
            ...obj,
            ...{ isComplete: false }
        };
    }

    public apiReturn: ApiResToComponent<E>;
}
