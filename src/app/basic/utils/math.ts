//  import * as MathUtil from '';

/**
 * 小数加法
 *
 * @param arg1
 * @param arg2
 */
export function decAdd(arg1: number, arg2: number) {
  let r1, r2, m, c;
  try {
    r1 = arg1.toString().split('.')[1].length;
  } catch (e) {
    r1 = 0;
  }
  try {
    r2 = arg2.toString().split('.')[1].length;
  } catch (e) {
    r2 = 0;
  }
  c = Math.abs(r1 - r2);
  m = Math.pow(10, Math.max(r1, r2));
  if (c > 0) {
    const cm = Math.pow(10, c);
    if (r1 > r2) {
      arg1 = Number(arg1.toString().replace('.', ''));
      arg2 = Number(arg2.toString().replace('.', '')) * cm;
    } else {
      arg1 = Number(arg1.toString().replace('.', '')) * cm;
      arg2 = Number(arg2.toString().replace('.', ''));
    }
  } else {
    arg1 = Number(arg1.toString().replace('.', ''));
    arg2 = Number(arg2.toString().replace('.', ''));
  }
  return (arg1 + arg2) / m;
}

/**
 * 小数减法
 *
 * @param arg1
 * @param arg2
 */
export function decSub(arg1: number, arg2: number) {
  let r1, r2, m, n;
  try {
    r1 = arg1.toString().split('.')[1].length;
  } catch (e) {
    r1 = 0;
  }
  try {
    r2 = arg2.toString().split('.')[1].length;
  } catch (e) {
    r2 = 0;
  }
  m = Math.pow(10, Math.max(r1, r2));
  n = r1 >= r2 ? r1 : r2;
  return Number(((arg1 * m - arg2 * m) / m).toFixed(n));
}

/**
 * 小数乘法
 *
 * @param arg1
 * @param arg2
 */
export function decMul(arg1: number, arg2: number) {
  let m = 0,
    s1 = arg1.toString(),
    s2 = arg2.toString();
  try {
    m += s1.split('.')[1].length;
  } catch (e) {}
  try {
    m += s2.split('.')[1].length;
  } catch (e) {}
  return (Number(s1.replace('.', '')) * Number(s2.replace('.', ''))) / Math.pow(10, m);
}

/**
 * 小数除法
 *
 * @param arg1
 * @param arg2
 */
export function decDiv(arg1: number, arg2: number) {
  let t1 = 0,
    t2 = 0,
    r1,
    r2;
  try {
    t1 = arg1.toString().split('.')[1].length;
  } catch (e) {}
  try {
    t2 = arg2.toString().split('.')[1].length;
  } catch (e) {}
  r1 = Number(arg1.toString().replace('.', ''));
  r2 = Number(arg2.toString().replace('.', ''));
  return (r1 / r2) * Math.pow(10, t2 - t1);
}

/**
 * 判断是否是整数
 *
 * @param obj
 */
export function isInteger(obj: any) {
  return typeof obj === 'number' && obj % 1 === 0;
}

/**
 * 获取最大值
 *
 * @returns number
 */
export function getMaxOfArray(array: number[]) {
  const getMax = (prev: number, next: number) => {
    return Math.max(prev, next);
  };
  return array.reduce(getMax);
}

/**
 * 获取小数位数
 * @param val
 */
export function getPointLength(val: number): number {
  const y = String(val).indexOf('.') + 1; //获取小数点的位置
  if (y === 0) {
    return 0;
  }
  return String(val).length - y; //获取小数点后的个数
}
