export function isObjectValueEqual(a: any, b: any): boolean {
  const aProps = Object.getOwnPropertyNames(a);
  const bProps = Object.getOwnPropertyNames(b);
  if (aProps.length !== bProps.length) {
    return false;
  }
  // tslint:disable-next-line: prefer-for-of
  for (let i = 0; i < aProps.length; i++) {
    const propName = aProps[i];

    const propA = a[propName];
    const propB = b[propName];
    // 2020-11-18更新，这里忽略了值为undefined的情况
    // 故先判断两边都有相同键名
    if (!b.hasOwnProperty(propName)) {
      return false;
    }
    if (typeof propA === 'object') {
      if (isObjectValueEqual(propA, propB)) {
        // return true     这里不能return ,后面的对象还没判断
      } else {
        return false;
      }
    } else if (propA !== propB) {
      return false;
    } else {
    }
  }
  return true;
}
