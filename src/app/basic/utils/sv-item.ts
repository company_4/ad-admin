import { deepGet, isNum } from '@delon/util';

export interface DelonSVItemsRule {
  label: string; // 显示的字段
  path?: string; // 获取值的路径
  unit?: string; // 单位
  enum?: { [key: number]: any }; // 管道enum的解析对象
  float?: number; // 小数部分长度
  extra?: string; // 额外显示的字符
}

// 用于循环解析的数据结构
export interface DelonSVItemOption {
  label: string; // 显示的字段
  text?: string; // 获取值的路径
  unit?: string; // 单位
}

/**
 * 解析并生成sv对象列表
 * @param rules
 * @param data
 */
export function makeDelonSVItems(rules: DelonSVItemsRule[], data: any): DelonSVItemOption[] | null {
  if (!rules || rules.length === 0 || !data) {
    return null;
  }
  // tslint:disable-next-line: prefer-const
  let res = [] as DelonSVItemOption[];
  rules.forEach(item => {
    let str = item.path ? deepGet(data, item.path, '') : '';
    if (item.float && isNum(str)) {
      str = str.toFixed(item.float);
    }
    if (item.extra) {
      str = `${str} ${item.extra}`;
    }
    if (item.enum) {
      str = item.enum[str];
    }
    res.push({
      label: item.label,
      text: str,
      unit: item.unit
    });
  });
  return res;
}
