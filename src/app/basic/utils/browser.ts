/**
 * 判断是否是IE浏览器和浏览器版本
 */
export function IEVersion(): { isIe: boolean; version: string | number } {
  const userAgent = navigator.userAgent; // 取得浏览器的userAgent字符串
  const isIE = userAgent.indexOf('compatible') > -1 && userAgent.indexOf('MSIE') > -1; // 判断是否IE<11浏览器
  const isEdge = userAgent.indexOf('Edge') > -1 && !isIE; // 判断是否IE的Edge浏览器
  const isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf('rv:11.0') > -1;
  if (isIE) {
    const reIE = new RegExp('MSIE (\\d+\\.\\d+);');
    reIE.test(userAgent);
    // tslint:disable-next-line: no-string-literal
    const fIEVersion = parseFloat(RegExp['$1']);
    if (fIEVersion === 7) {
      return {
        isIe: true,
        version: 7
      };
    } else if (fIEVersion === 8) {
      return {
        isIe: true,
        version: 8
      };
    } else if (fIEVersion === 9) {
      return {
        isIe: true,
        version: 9
      };
    } else if (fIEVersion === 10) {
      return {
        isIe: true,
        version: 10
      };
    } else {
      return {
        isIe: true,
        version: 6
      }; // IE版本<=7
    }
  } else if (isEdge) {
    return {
      isIe: false,
      version: 'edge'
    }; // edge
  } else if (isIE11) {
    return {
      isIe: true,
      version: 11
    }; // IE11
  } else {
    return {
      isIe: false,
      version: -1
    }; // 不是ie浏览器
  }
}
