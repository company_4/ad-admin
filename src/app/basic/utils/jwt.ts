import { JWTTokenModel } from '@delon/auth';

/**
 * 用一个类来衔接处理token
 */
export class JWTTokenModelHelper extends JWTTokenModel {
  constructor(token: string) {
    super();
    this.token = token;
  }
}
