/**
 * 判断是否需要将新的字符安串加入到list
 */
export function checkAndPushStringArray(list: string[], str: string): string[] {
  if (list.length === 0 || list.indexOf(str) < 0) {
    list.push(str);
  }
  return list;
}
