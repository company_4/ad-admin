import { Observable, Subject } from 'rxjs';

/**
 * 网络请求、事件操作节流，在短时间内，不发出多个网络请求，只会执行第一次
 * 针对每一个网络请求，设置一个网络请求拦截器
 */
export class ActionHolder {
  // 节流时间
  private throttleTime: number;

  /**
   * 上次执行时间
   */
  private lastTime: number | null = null;

  constructor(throttleTime?: number) {
    this.throttleTime = throttleTime || 500;
  }

  private _action = new Subject<any>();

  private _params?: any;

  get params(): any {
    return this._params;
  }

  get action(): Observable<any> {
    return this._action.asObservable();
  }

  // 如果有变动，则触发变动
  nextStep(): void {
    this._action.next(this.params);
  }

  // 表单值变动后，判定是否可以触发变动
  doAction(params: any): void {
    // 如果没有上次时间，直接执行
    if (!this.lastTime) {
      this.lastTime = new Date().getTime();
      this._params = params;
      this.nextStep();
    }
    // 如果有上次执行时间，且时间在间隔时间内
    if (this.lastTime + this.throttleTime <= new Date().getTime()) {
      this.lastTime = new Date().getTime();
      this._params = params;
      this.nextStep();
    }
  }
}
