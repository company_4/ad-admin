import { MessageReceiveData, MessageSendData } from './interface';

export type ArgumentsType<T> = T extends (...args: infer U) => void ? U : never;

export type SendArgumentsType<T extends keyof MessageSendData> = MessageSendData[T] extends never
  ? ArgumentsType<(action: T) => void>
  : ArgumentsType<(action: T, data: MessageSendData[T]) => void>;

export type ReceiveArgumentsType<T extends keyof MessageReceiveData> = MessageReceiveData[T] extends undefined
  ? () => void
  : (data?: MessageReceiveData[T]) => void;
