# Typescript Websocket

这是一个在使用 Websocket 时根据消息类型来约束数据类型的服务

## 菜单

interface // 定义不同的消息类型，并规定数据映射约定消息格式类型。根据项目实际需要可对类型进行修改
message-listeners-manager // 创建订阅管理器，将每个类型的消息单独用一个方法进行管理，并不再关心如何取消订阅
message-listeners // 创建订阅装饰器，创建指定消息类型的订阅
message-ts-util // 规定被装饰的方法的参数类型
message.service // 定义 WebSocket 服务，在该文件中对服务端 url 进行设置

## 使用

在需要使用该服务的组件中继承 MessageListenersManager 即可使用。
装饰器通过 @MessageListener 进行使用

### 装饰器代码示例

```typescript
@MessageListener(Receive.UPDATE)
updateData(message: string): void {

}
```

## 参考资料

[https://github.com/hsuanxyz/ts-websocket]
