import { Inject, Injectable } from '@angular/core';
import { DA_SERVICE_TOKEN, ITokenModel, ITokenService } from '@delon/auth';
import { SettingsService } from '@delon/theme';

@Injectable({ providedIn: 'root' })
export class UserService {
  // 是否锁定屏幕
  public isLockScreen: boolean | any;

  // 是否需要加载APP初始化信息
  public isNeedInitApp: boolean | any;

  // store the URL so we can redirect after logging in
  redirectUrl = '/';

  // 获取登录地址
  get loginUrl(): string | undefined {
    return this.tokenSrv.login_url;
  }

  // 获取token对象
  get item(): ITokenModel | null {
    return this.tokenSrv.get();
  }

  // 获取token，判断是否登录
  get isLogin(): boolean {
    if (!this.item) {
      return false;
    }
    return !!this.item.token;
  }

  // 获取登录用户姓名
  get name(): string | undefined {
    return this.settingsSrv.user.name;
  }

  // 获取登录用户邮箱
  get email(): string | undefined {
    return this.settingsSrv.user.email;
  }

  // 获取登录用户头像
  get avatar(): string | undefined {
    return this.settingsSrv.user.avatar;
  }

  constructor(@Inject(DA_SERVICE_TOKEN) private tokenSrv: ITokenService, private settingsSrv: SettingsService) {}

  // 登出
  logout(): void {
    this.tokenSrv.clear();
  }
}
