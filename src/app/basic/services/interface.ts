/**
 * Log输出颜色配置等
 */
export interface LogServiceParams {
    msg?: string; // 文字描述
    color?: string; // 文字描述颜色
    obj?: any; // Log输出内容
}

// 用户登录时传递的基本信息
export interface UserLoginParams {
    userMark: string | number;
    passWord: string | number;
    appType?: 'web_login' | string | number;
    appId?: string | number;
    appSecret?: string | number;
}

// 用户的基本信息
export interface UserInfo {
    name?: string;
    email?: string;
    avatar?: string;
    [key: string]: any;
}

// ng-alain存储的用户基本信息结构
export interface UserInfoAndToken {
    token?: string;
    user?: UserInfo;
}

/**
 * -----------------作屏幕锁定时的用户身份验证----------------------
 */
// 验证用户密码时传递的信息（ng-alain的HTTP拦截器会携带Token到服务器，根据Token判定用户ID）
export interface VerifyUserPassWordParmas {
    passWord: string | number;
}

// 用户密码是否正确
export interface IsPassWordWright {
    isPassWdRight: boolean;
}

/**
 * 应用初始化时返回的信息
 */
export interface AppCommConfig {
    apiUrl: string; // 服务器域名
    appId: string; // appId
    publicKey: string; // 加密公钥
    encryptPW?: boolean; // 密码是否加密
    helpDocUrl?: string; // 帮助文档地址
}

/**
 * 初始化配置信息
 */
export interface AppInitConfig {
    appId: string; // 应用ID
    appInitServer: string; // 初始化服务器域名IP
    apiUrl: string; // 服务器域名
}
