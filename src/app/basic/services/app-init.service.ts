import { Inject, Injectable } from '@angular/core';
import { I18NService } from '@core';
import { ALAIN_I18N_TOKEN, SettingsService, TitleService, _HttpClient } from '@delon/theme';

import { UserService } from './user.service';
import { LoggerService } from '@basic/services/logger.service';

@Injectable({ providedIn: 'root' })
export class AppInitService {
    // 允许直接访问
    direct: any = { _allow_anonymous: true };

    constructor(
        private httpClient: _HttpClient,
        public titleService: TitleService,
        public settingService: SettingsService,
        public log: LoggerService,
        private userSer: UserService,
        @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService
    ) {}

    //   /**
    //    * 获取APP的初始化信息，不包含用户信息，菜单，权限等
    //    */
    //   appInit(): Promise<any> {
    //     if (this.userSer.isNeedInitApp) {
    //       return new Promise((resolve, reject) => {
    //         this.userSer.isNeedInitApp = false;
    //         // 加个请求，将翻译的工作也做了，保障翻译的完整性
    //         this.httpClient.get(`assets/tmp/i18n/${this.i18n.defaultLang}.json`).subscribe(
    //           (langData) => {
    //             // setting language data
    //             this.translate.setTranslation(this.i18n.defaultLang, langData);
    //             this.translate.setDefaultLang(this.i18n.defaultLang);

    //             // application data
    //             const _appInfo: any = environment.appSetting;
    //             // 应用信息：包括站点名、描述、年份
    //             this.settingService.setApp(_appInfo);
    //             // 设置页面标题的后缀
    //             this.titleService.suffix = _appInfo.name;
    //           },
    //           () => {},
    //           () => {
    //             resolve(null);
    //           },
    //         );
    //         resolve(null);
    //       });
    //     }

    //     return new Promise((resolve, reject) => {
    //       resolve(null);
    //     });
    //   }
    // }

    // if (!this.checkIfToken()) {
    //     return new Promise((resolve) => {
    //         this.httpClient.get(`assets/tmp/i18n/${this.i18n.defaultLang}.json`)
    //             .subscribe(
    //                 (langData) => {
    //                     // setting language data
    //                     this.translate.setTranslation(this.i18n.defaultLang, langData);
    //                     this.translate.setDefaultLang(this.i18n.defaultLang);
    //                     // application data
    //                     this.settingService.setApp(AppSetting);
    //                     // 设置页面标题的后缀
    //                     this.titleService.default = '';
    //                     this.titleService.suffix = AppSetting.name;
    //                 },
    //                 () => { },
    //                 () => {
    //                     resolve(null);
    //                 },
    //             );
    //         resolve(null);
    //     });
    // }
}
