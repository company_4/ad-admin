import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { share } from 'rxjs/operators';

/**
 * 登录信息变更通知
 */
@Injectable({ providedIn: 'root' })
export class AccountChangeService implements OnDestroy {
    private change$ = new BehaviorSubject<null>(null);

    get change(): Observable<null> {
        return this.change$.pipe(share());
    }

    /**
     * 变更对象
     */
    changeAccount() {
        this.change$.next(null);
    }

    ngOnDestroy(): void {
        this.change$.unsubscribe();
    }
}
