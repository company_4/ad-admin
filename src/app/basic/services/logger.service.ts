import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

export interface LogAndColor {
    msg: string | number;
    color: string;
}

/**
 * Log输出打印服务
 * TODO: 后期将日志写入服务器或加入到浏览器数据库
 */
@Injectable({ providedIn: 'root' })
export class LoggerService {
    constructor() {}

    /**
     * 写有颜色标记的文字提示信息
     * @param msg string
     * @param color string
     */
    public logMsg(msg: string | number = '', color: string = 'red'): void {
        if (!environment.production) {
            console.log(`%c ${msg}`, `background:${color};color:#fff`);
        }
    }

    /**
     * 打印数据信息
     * @param obj any
     * @param msg string
     * @param color string
     */
    public logObj(obj: any, msg: string | number = '', color: string = 'red'): void {
        if (!environment.production) {
            console.log(`%c ${msg} ==>>`, `background:${color};color:#fff`, obj);
            // console.log(obj);
        }
    }
}
