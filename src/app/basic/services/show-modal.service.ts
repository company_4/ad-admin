import { Inject, Injectable, Injector } from '@angular/core';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { ModalHelper } from '@delon/theme';
import { AlainAuthConfig } from '@delon/util';
import { ModalOptions, NzModalService } from 'ng-zorro-antd/modal';
import { Observable } from 'rxjs';

/**
 * Modal弹出服务
 */
@Injectable({ providedIn: 'root' })
export class ShowModalService {
    private get cog(): AlainAuthConfig {
        return this.srv.options;
    }

    private get tokenSrv(): ITokenService {
        return this.injector.get(DA_SERVICE_TOKEN);
    }

    constructor(
        @Inject(DA_SERVICE_TOKEN) private srv: ITokenService,
        private service: NzModalService,
        private helper: ModalHelper,
        private injector: Injector
    ) {}

    /**
     * showModalOfSuccess
     *
     * @param msg any
     */
    public showModalOfSuccess(msg: any): void {
        this.service.success({
            nzTitle: `操作成功！`,
            nzZIndex: 1200,
            nzContent: msg
        });
    }

    /**
     * showModalOfErrorFromArray
     *
     * @param arraymsg string|any
     */
    // public showModalOfErrorFromArray(msg: any): void {
    //     let str = '';
    //     if (msg instanceof Array) {
    //         for (const key of Object.keys(msg)) {
    //             str += `${msg[key]}<br>`;
    //         }
    //     } else {
    //         str += msg;
    //     }
    //     this.showModalOfError(str);
    // }

    /**
     * showModalOfError
     *
     * @param msg string
     */
    public showModalOfError(msg: any): void {
        this.service.error({
            nzTitle: `操作失败！`,
            nzZIndex: 1200,
            nzContent: msg
        });
    }

    /**
     * showModalOfInfo
     *
     * @param msg string
     */
    public showModalOfInfo(msg: any): void {
        this.service.info({
            nzTitle: `重要提示！`,
            nzZIndex: 1200,
            nzContent: msg
        });
    }

    /**
     * 构建静态框，点击蒙层不允许关闭
     *
     * @param comp 模态框内部结构
     * @param params 传递到模态框的参数
     * @param size 模态框尺寸
     * @param options 模态框体参数
     */
    public creatStaticModal(
        comp: any,
        params?: any,
        size: 'sm' | 'md' | 'lg' | 'xl' | '' | number = 'lg',
        options?: ModalOptions
    ): Observable<any> {
        const modalOptions = { nzMaskClosable: false, ...options };
        return this._creatModal(comp, params, size, modalOptions);
    }

    /**
     * 构建一个对话框
     *
     * @param comp 模态框内部结构
     * @param params 传递到模态框的参数
     * @param size 模态框尺寸
     * @param options 模态框体参数
     */
    public _creatModal(
        comp: any,
        params?: any,
        size: 'sm' | 'md' | 'lg' | 'xl' | '' | number = 'lg',
        options?: ModalOptions
    ): Observable<any> {
        /**
         * 创建modal前先判断token是否已经过期，或者是否需要重新更新token
         */
        // if (this.checkTokenIsOffset()) //
        /**
         * 直接关闭modal
         * ngOnDestroy(): void {
         * this.modalSrv.closeAll();
         * }
         */
        // if (this.checkTokenIsOffset())
        return this.helper.createStatic(comp, params, {
            size,
            drag: true,
            modalOptions: options,
            exact: false
        });
    }

    /**
     * 构建一个对话框
     *
     * @param comp 模态框内部结构
     * @param params 传递到模态框的参数
     * @param size 模态框尺寸
     * @param options 模态框体参数
     */
    public creatModal(
        comp: any,
        params?: any,
        size: 'sm' | 'md' | 'lg' | 'xl' | '' | number = 'lg',
        options?: ModalOptions
    ): Observable<any> {
        /**
         * 创建modal前先判断token是否已经过期，或者是否需要重新更新token
         */
        // if (this.checkTokenIsOffset()) //
        /**
         * 直接关闭modal
         * ngOnDestroy(): void {
         * this.modalSrv.closeAll();
         * }
         */
        // if (this.checkTokenIsOffset())
        return this.helper.create(comp, params, {
            size,
            drag: true,
            modalOptions: options,
            exact: false
        });
    }

    public confirm(option: ModalOptions): void {
        this.service.confirm(option);
    }
}
