import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { _HttpClient } from '@delon/theme';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiResponse, ApiResToComponent, ErrorApiRequest, SuccessApiRequest } from '../datas';
import { AppInitService } from './app-init.service';
import { CommBasicService } from './basic.service';
import { VerifyUserPassWordParmas } from './interface';
import { UserService } from './user.service';

@Injectable({ providedIn: 'any' })
export class AppAuthService extends CommBasicService {
  // // 直接访问
  // direct: any = { _allow_anonymous: true };
  // constructor(public override injector: Injector, private router: Router, public userSer: UserService, private appInit: AppInitService) {
  //     super(injector);
  // }
  // // 登录用户
  // login(body: UserLoginParams): Observable<ApiResToComponent> {
  //     return this.client.post(this.apiUrl.OAuth.login, body, this.direct).pipe(
  //         map((ev: ApiResponse) => {
  //             return this.doWithResponse(
  //                 ev,
  //                 {
  //                     msg: 'APP登录用户信息',
  //                     color: 'green',
  //                 },
  //                 true,
  //             );
  //         }),
  //     );
  // }
  // // 检查用户是否登录，验证token
  // checkToken(): Observable<ApiResToComponent> {
  //     return this.client.put(this.apiUrl.OAuth.checktoken).pipe(
  //         map((ev: ApiResponse) => {
  //             this.log.logObj(ev, '验证用户Token密码是否正确', 'red');
  //             if (ev.code !== 0) {
  //                 this.msg.error(ev.message);
  //                 // 获取登录地址
  //                 const loginUrl = this.userSer.loginUrl;
  //                 // 验证失败，返回登录界面
  //                 this.appInit.appInit().then(() => this.router.navigate([loginUrl]));
  //                 return new ErrorApiRequest().apiReturn;
  //             } else {
  //                 return new SuccessApiRequest(ev).apiReturn;
  //             }
  //         }),
  //     );
  // }
  // // 验证用户密码是否正确，用于开启锁屏界面
  // verifyUserPassWord(body: VerifyUserPassWordParams): Observable<ApiResToComponent> {
  //     return this.client.put(this.apiUrl.OAuth.verifyPassword, body).pipe(
  //         map((ev: ApiResponse) => {
  //             return this.doWithResponse(ev, {
  //                 msg: '验证用户密码是否正确',
  //                 color: 'red',
  //             });
  //         }),
  //     );
  // }
  // logout(param: any): Observable<ApiResToComponent> {
  //     return this.client.get(this.apiUrl.OAuth.logout, param).pipe(
  //         map((ev: ApiResponse) => {
  //             this.log.logObj(ev, '用户登出', 'red');
  //             if (ev.code !== 0) {
  //                 return new ErrorApiRequest().apiReturn;
  //             } else {
  //                 return new SuccessApiRequest(ev).apiReturn;
  //             }
  //         }),
  //     );
  // }
}
