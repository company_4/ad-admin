export interface NumberRules {
  max: number;
  min: number;
  float: number;
  item: string;
}
