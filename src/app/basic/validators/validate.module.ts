import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { NumberValidatorDirective } from './number-validate';

const VALIDATE = [NumberValidatorDirective];

@NgModule({
  imports: [CommonModule],
  providers: [],
  declarations: [...VALIDATE],
  exports: [...VALIDATE]
})
export class BasicValidatorsModule {}
