import { Injectable, Injector } from '@angular/core';

import { AppConfigService } from '../services';

@Injectable({ providedIn: 'root' })
export abstract class AbstractApiUrlDataService {
    // 获取API地址
    get apiHost(): string {
        return this.aConfigSrv.appApiHost;
    }

    // 接口版本
    _version = '';

    baseUrl = this.apiHost + this._version;

    protected constructor(public injector: Injector, public aConfigSrv: AppConfigService) {}
}
