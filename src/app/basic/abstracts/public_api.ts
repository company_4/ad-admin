export { BaseOfSimpleTable } from './abstract-simple-table.base';
export { BaseOfSimpleForm } from './abstract-simple-form.base';
export { UrlOfSimpleTable } from './abstract-simple-table.url';
export { BaseOfSimpleComponent } from './abstract-simple-component.base';
export { AbstractApiUrlDataService } from './abstract-api-url.service';

export * from './interface';
