import { Directive, Injector } from '@angular/core';
import { ApiResToComponent } from '@basic';
import { STColumn } from '@delon/abc/st';
import { Observable } from 'rxjs';

import { BaseOfSimpleComponent } from './abstract-simple-component.base';

/**
 * 根据url请求所有数据后再赋值到表格
 */
@Directive()
export abstract class BaseOfSimpleTable extends BaseOfSimpleComponent {
    // 表格数据列表
    public dataList = [] as any;

    // 请求数据的参数
    params: any = {};

    abstract columns: STColumn[];

    protected constructor(public override injector: Injector) {
        super(injector);
    }

    // 根据条件搜索数据
    startSearchData(params: any): void {
        this.sendDataToList(this.getListDataOfRules(params));
    }

    // 重新加载数据
    reLoadData(params: any): void {
        this.sendDataToList(this.getAllListData(params));
    }

    // 根据条件搜索信息
    abstract getListDataOfRules(params: any): Observable<ApiResToComponent<any>>;

    // 获取默认的所有数据
    abstract getAllListData(params: any): Observable<ApiResToComponent<any>>;

    // 将数据赋值到数组
    sendDataToList(observable: Observable<ApiResToComponent<any>>): void {
        this.loading = true;
        observable.subscribe((response: ApiResToComponent<any>) => {
            this.loading = false;
            if (response.isComplete) {
                this.dataList = this.toList(response.data);
                this.cdRef.markForCheck();
            }
        });
    }

    // toList
    toList(items: any): any[] {
        return items.list;
    }
}
