import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringCut'
})
export class StringCutPipe implements PipeTransform {
  /**
   * 按长度截取字符串
   * @param str string
   * @param length number
   */
  transform(str: string, length: number | 10): string {
    if (typeof str !== 'string') {
      return str;
    }
    if (typeof str === 'string') {
      if (str.length > length) {
        return `${str.substring(length, 0)}...`;
      } else {
        return str;
      }
    }
    return str;
  }
}
