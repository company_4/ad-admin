import { Pipe, PipeTransform } from '@angular/core';

/**
 * 是非判断
 */
@Pipe({
  name: 'boolean'
})
export class TrueFalsePipe implements PipeTransform {
  /**
   * 输出是或否
   * @param value 输入的值
   */
  transform(value: string | number | boolean | undefined): string | undefined {
    if (typeof value === 'string') {
      return value;
    }
    if (typeof value === 'number') {
      if (value !== 0) {
        return `是`;
      } else {
        return `否`;
      }
    }
    if (typeof value === 'boolean') {
      if (value) {
        return `是`;
      } else {
        return `否`;
      }
    }
    if (!value) {
      return value;
    }
    return `否`;
  }
}
