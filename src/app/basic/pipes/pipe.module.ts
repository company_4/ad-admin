import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TrueFalsePipe } from './boolean.pipe';
import { EnumPipe } from './enum.pipe';
import { HtmlPipe } from './html.pipe';
import { MinuteToStringPipe } from './minute.pipe';
import { RelativeTimePipe } from './relative-time.pipe';
import { SexIdentifyPipe } from './sex-identify.pipe';
import { StringCutPipe } from './string-cut.pipe';
import { TimesPipe } from './times.pipe';

const PIPES = [TrueFalsePipe, EnumPipe, SexIdentifyPipe, StringCutPipe, TimesPipe, RelativeTimePipe, HtmlPipe, MinuteToStringPipe];

@NgModule({
  imports: [CommonModule],
  providers: [],
  declarations: [...PIPES],
  exports: [...PIPES]
})
export class BasicPipesModule {}
