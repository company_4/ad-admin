import { Pipe, PipeTransform } from '@angular/core';

/**
 * 枚举
 */
@Pipe({
    name: 'enum'
})
export class EnumPipe implements PipeTransform {
    /**
     * 枚举
     * @param key string
     * @param enums any[]
     */
    transform(key: string, enums: { [key: number]: any }): any {
        // 直接返回枚举的值
        return enums[key as any] || key;
    }
}
