import { Pipe, PipeTransform } from '@angular/core';

/**
 * 时间判断
 */
@Pipe({
  name: 'minute'
})
export class MinuteToStringPipe implements PipeTransform {
  /**
   * 输出是或否
   * @param minute
   */
  transform(minute: number): string {
    if (minute < 60) return `${minute}分`;
    if (minute === 60) return `小时`;
    if (minute < 1440) {
      //整数部分
      const hour = Math.floor(minute / 60);
      let str = `${hour}小时`;
      //余数部分
      const _minute = minute % 60;
      if (_minute !== 0) {
        str = `${str}${_minute}分`;
      }
      return str;
    }
    if (minute === 1440) return `天`;
    if (minute > 1440) {
      const day = Math.floor(minute / 1440);
      const left = minute - 1440 * day;
      let str = `${left}天`;
      if (left !== 0) {
        //整数部分
        const hour = Math.floor(left / 60);
        if (hour !== 0) {
          str = `${str}${hour}小时`;
        }
        //余数部分
        const _minute = left % 60;
        if (_minute !== 0) {
          str = `${str}${_minute}分`;
        }
      }
      return str;
    }
    return `${minute}分`;
  }
}
