import { Pipe, PipeTransform } from '@angular/core';
import { formatDistanceToNow } from 'date-fns';

@Pipe({
  name: 'relativeTime'
})
export class RelativeTimePipe implements PipeTransform {
  transform(value: number | string | Date): any {
    return value ? formatDistanceToNow(new Date(value)) : '';
  }
}
