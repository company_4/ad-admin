import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DelonFormModule, WidgetRegistry } from '@delon/form';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSwitchModule } from 'ng-zorro-antd/switch';

import { AddressWidget } from './widgets/address/address.widget';
import { AntdIconComponent } from './widgets/antd-icon/antd-icon.component';
import { EditorWidget } from './widgets/editor/editor.widget';
import { ImgWidget } from './widgets/img/img.widget';
import { PasswordWidget } from './widgets/password/password.widget';
import { SwitchWidget } from './widgets/switch/switch.widget';
import { AddressModule } from '../components/address';
import { EditorModule } from '../components/editor';
import { FileManagerModule } from '../components/file-manager';
import {NzGridModule} from "ng-zorro-antd/grid";

export const SCHEMA_THIRDS_COMPONENTS = [EditorWidget, ImgWidget, AddressWidget, SwitchWidget, PasswordWidget, AntdIconComponent];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DelonFormModule.forRoot(),
        AddressModule,
        EditorModule,
        FileManagerModule,
        NzButtonModule,
        NzSwitchModule,
        NzInputModule,
        NzIconModule,
        NzDropDownModule,
        NzGridModule
    ],
    declarations: SCHEMA_THIRDS_COMPONENTS,
    exports: [...SCHEMA_THIRDS_COMPONENTS]
})
export class JsonSchemaModule {
    constructor(widgetRegistry: WidgetRegistry) {
        widgetRegistry.register(EditorWidget.KEY, EditorWidget);
        widgetRegistry.register(PasswordWidget.KEY, PasswordWidget);
        widgetRegistry.register(SwitchWidget.KEY, SwitchWidget);
        widgetRegistry.register(ImgWidget.KEY, ImgWidget);
        widgetRegistry.register(AddressWidget.KEY, AddressWidget);
        widgetRegistry.register(AntdIconComponent.KEY, AntdIconComponent);
    }
}
