import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ControlUIWidget, SFValue } from '@delon/form';

import { AntdIconWidgetSchema, ICONS_LIST } from './schema';

@Component({
    selector: 'app-antd-icon',
    templateUrl: './antd-icon.component.html',
    styleUrls: ['./antd-icon.component.less'],
    encapsulation: ViewEncapsulation.None
})
export class AntdIconComponent extends ControlUIWidget<AntdIconWidgetSchema> {
    static readonly KEY = 'icon';

    icons = ICONS_LIST;

    nzVisible: boolean = false;

    searchValue: string = '';

    /**
     * 按钮的值
     */
    iconString: string = '';

    /**
     * 搜索的值变更
     * @param event
     */
    changeSearchValue(event: any) {
        this.icons = ICONS_LIST.filter(xx => xx.indexOf(event) !== -1);
        this.cd.detectChanges();
    }

    checkIcon(iconString: string) {
        this.iconString = iconString;
        this.cd.detectChanges();
        this.setValue(iconString);
    }

    override reset(value: SFValue): void {
        this.iconString = value;
    }
}
