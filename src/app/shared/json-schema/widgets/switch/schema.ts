import type { SFUISchemaItem } from '@delon/form';

export interface SwitchWidgetSchema extends SFUISchemaItem {
    checkedValue?: any;
    unCheckedValue?: any;
    checkedChildren?: any;
    unCheckedChildren?: any;
}
