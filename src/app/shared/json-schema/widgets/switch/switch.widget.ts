import { Component, ViewEncapsulation } from '@angular/core';
import { ControlUIWidget, SFValue } from '@delon/form';

import { SwitchWidgetSchema } from './schema';

/**
 * 利用switch组件，实现两个相对值的调用
 */
@Component({
    selector: 'sf-switch',
    templateUrl: './switch.widget.html',
    preserveWhitespaces: false,
    encapsulation: ViewEncapsulation.None
})
// tslint:disable-next-line:component-class-suffix
export class SwitchWidget extends ControlUIWidget<SwitchWidgetSchema> {
    static readonly KEY = 'switch';

    /**
     * 说明
     * ui: {
     *  checkedChildren: '开',
     *  checkedValue: '1',
     *  unCheckedChildren: '关',
     *  unCheckedValue: '0',
     * }
     */

    /**
     * switch的默认值
     */
    _value?: boolean;

    /**
     * 重新赋值
     */
    override reset(value: SFValue): void {
        if (Number(this.ui.checkedValue) === Number(value)) {
            this._value = true;
        }
    }

    /**
     * 处理传递过来的值，并返回结果
     *
     * @param value boolean
     */
    _change(value: boolean): void {
        const res = Number(this.ui.checkedValue) === Number(value) ? this.ui.checkedValue : this.ui.unCheckedValue;
        this.setValue(res);
    }
}
