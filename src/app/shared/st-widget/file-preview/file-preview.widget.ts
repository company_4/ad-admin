import { ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
    selector: 'app-file-preview',
    templateUrl: './file-preview.widget.html',
    styleUrls: ['./file-preview.widget.less']
})
export class FilePreviewWidget implements OnInit {
    static readonly KEY = 'file-preview';
    /**
     * 文件地址
     */
    fileUrl!: string;
    /**
     * 文件后缀
     */
    fileSuffix!: string;

    showImgFile: boolean = false;

    constructor(
        private cdr: ChangeDetectorRef,
        private injector: Injector
    ) {}

    private get msg(): NzMessageService {
        return this.injector.get(NzMessageService);
    }
    checkFileSuffix(fileSuffix: string) {
        let arr = ['png', 'jpg', 'jpeg'];
        return arr.some(type => {
            return fileSuffix.indexOf(type) > -1;
        });
    }

    ngOnInit() {
        this.showImgFile = this.checkFileSuffix(this.fileSuffix);
        this.cdr.detectChanges();
    }

    copyItem() {
        const aux = document.createElement('input');
        aux.setAttribute('value', this.fileUrl);
        document.body.appendChild(aux);
        aux.select();
        document.execCommand('copy');
        document.body.removeChild(aux);
        this.msg.success('复制成功');
    }
}
