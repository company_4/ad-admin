import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
    selector: 'st-widget-long-text',
    template: `<span nz-tooltip [nzTooltipTitle]="text">{{ text }}</span>`,
    host: {
        '(dblclick)': 'copyItem()'
    },
    styles: [
        `
            span {
                display: block;
                -webkit-text-overflow: ellipsis;
                -moz-text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden !important;
                -o-text-overflow: ellipsis;
                text-overflow: ellipsis !important;
            }
        `
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class STLongTextWidget {
    static readonly KEY = 'long-text';

    // 文字内容
    text!: string;

    constructor(private injector: Injector) {}

    private get msg(): NzMessageService {
        return this.injector.get(NzMessageService);
    }

    copyItem() {
        const aux = document.createElement('input');
        aux.setAttribute('value', this.text);
        document.body.appendChild(aux);
        aux.select();
        document.execCommand('copy');
        document.body.removeChild(aux);
        this.msg.success('复制成功');
    }
}
