import { NgModule } from '@angular/core';

// import { STWidgetRegistry } from '@delon/abc/st';
import { STWidgetRegistry } from '@delon/abc/st';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

import { STColorNumberWidget } from './color-number.widget';
import { FilePreviewWidget } from './file-preview/file-preview.widget';
import { STLongTextWidget } from './long-text.widget';
import { SharedModule } from '../shared.module';

export const STWIDGET_COMPONENTS = [STLongTextWidget, STColorNumberWidget, FilePreviewWidget];

@NgModule({
    declarations: STWIDGET_COMPONENTS,
    imports: [SharedModule, NzToolTipModule, NzImageModule],
    exports: [...STWIDGET_COMPONENTS]
})
export class STWidgetModule {
    constructor(widgetRegistry: STWidgetRegistry) {
        widgetRegistry.register(STLongTextWidget.KEY, STLongTextWidget);
        widgetRegistry.register(STColorNumberWidget.KEY, STColorNumberWidget);
        widgetRegistry.register(FilePreviewWidget.KEY, FilePreviewWidget);
    }
}
