import { Component, Injector, OnInit } from '@angular/core';

@Component({
    selector: 'st-widget-color-text',
    template: `<span [ngClass]="{ 'text-error': number > 0 }">{{ number.toFixed(2) }}</span>`,
    host: {
        // '(click)': 'handlePreview()'
    },
    styles: [``]
    // changeDetection: ChangeDetectionStrategy.OnPush
})
export class STColorNumberWidget {
    static readonly KEY = 'color-number';

    // 文字内容
    number!: number;

    constructor(private injector: Injector) {}
}
