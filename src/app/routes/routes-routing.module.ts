import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// layout
import { LayoutProComponent } from '@brand';
import { environment } from '@env/environment';
// dashboard pages
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutProComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', component: DashboardComponent },
            { path: 'user', loadChildren: () => import('@pages/user/user.module').then(m => m.UserModule) },
            { path: 'system', loadChildren: () => import('@routes/system/system.module').then(m => m.SystemModule) }
        ]
    },
    // passport
    { path: '', loadChildren: () => import('./passport/passport.module').then(m => m.PassportModule) },
    { path: 'exception', loadChildren: () => import('./exception/exception.module').then(m => m.ExceptionModule) },
    // 单页不包裹Layout
    { path: '**', redirectTo: 'exception/404' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            useHash: environment.useHash,
            // NOTICE: If you use `reuse-tab` component and turn on keepingScroll you can set to `disabled`
            // Pls refer to https://ng-alain.com/components/reuse-tab
            scrollPositionRestoration: 'top'
        })
    ],
    exports: [RouterModule]
})
export class RouteRoutingModule {}
