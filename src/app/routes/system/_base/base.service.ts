import { Injectable, Injector } from '@angular/core';
import { CommBasicService } from '@basic';

import { ApiUrlDataOfSystemService } from '../_url';

@Injectable()
export class SystemBasicService extends CommBasicService {
    apiUrl: ApiUrlDataOfSystemService;

    constructor(protected override injector: Injector) {
        super(injector);
        this.apiUrl = this.injector.get(ApiUrlDataOfSystemService);
    }
}
