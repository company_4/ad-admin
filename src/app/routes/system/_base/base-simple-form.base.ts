import { Directive, Injector } from '@angular/core';
import { BaseOfSimpleForm } from '@basic';
import { NzModalRef } from 'ng-zorro-antd/modal';

import { ApiUrlDataOfSystemService } from '../_url';
/**
 * 基础表单控件
 */
@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class BaseOfSystemForm<R, P> extends BaseOfSimpleForm<R, P> {
    apiUrl: ApiUrlDataOfSystemService;

    constructor(protected override injector: Injector) {
        super(injector);
        this.apiUrl = this.injector.get(ApiUrlDataOfSystemService);
    }
}
