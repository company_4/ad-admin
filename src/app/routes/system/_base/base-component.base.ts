import { Directive, Injector } from '@angular/core';
import { BaseOfSimpleComponent } from '@basic';

import { ApiUrlDataOfSystemService } from '../_url';

/**
 * 基础Component控件,主要处理一些基础通用的问题，比如，退出路由时，关闭modal
 */
@Directive()
export abstract class BaseOfSystemComponent extends BaseOfSimpleComponent {
    apiUrl: ApiUrlDataOfSystemService;

    constructor(protected override injector: Injector) {
        super(injector);
        this.apiUrl = this.injector.get(ApiUrlDataOfSystemService);
    }
}
