import { Directive, Injector } from '@angular/core';
import { UrlOfSimpleTable } from '@basic';
import { ApiUrlDataOfSystemService } from '../_url';

/**
 * 根据url请求单页数据，后台翻页
 */
@Directive()
export abstract class UrlOfSystemTable extends UrlOfSimpleTable {
    apiUrl: ApiUrlDataOfSystemService;

    constructor(protected override injector: Injector) {
        super(injector);
        this.apiUrl = this.injector.get(ApiUrlDataOfSystemService);
    }
}
