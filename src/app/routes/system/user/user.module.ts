import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';

import { UserListComponent } from './user-list/user-list.component';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { SystemEditUserComponent } from './edit-user/edit-user.component';

@NgModule({
    declarations: [UserListComponent, SystemEditUserComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index',
                component: UserListComponent
            }
        ]),
        NzPopconfirmModule,
        NzTreeModule,
        NzSkeletonModule
    ]
})
export class SystemUserModule {}
