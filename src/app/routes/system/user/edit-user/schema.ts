import { Injector } from '@angular/core';
import { SFSchema } from '@delon/form';
import { SystemDictService } from '@routes/system/dict/dict.service';
import { SystemUserService } from '@routes/system/user/user.service';

export function ADD_USER_SCHEMA(injector: Injector): SFSchema {
    return {
        properties: {
            nickName: {
                type: 'string',
                title: '用户昵称',
                ui: {
                    placeholder: '请输入用户昵称',
                    grid: {
                        span: 12
                    }
                },
                maxLength: 30
            },
            deptId: {
                type: 'string',
                title: '归属部门',
                ui: {
                    widget: 'tree-select',
                    placeholder: '请选择归属部门',
                    virtualHeight: '300px',
                    grid: {
                        span: 12
                    },
                    asyncData: () => injector.get(SystemUserService).deptTreeSelectSF()
                }
            },
            phonenumber: {
                type: 'string',
                title: '手机号码',
                format: 'mobile',
                ui: {
                    placeholder: '请输入手机号码',
                    grid: {
                        span: 12
                    }
                },
                maxLength: 11
            },
            email: {
                type: 'string',
                title: '邮箱',
                format: 'email',
                ui: {
                    placeholder: '请输入邮箱',
                    grid: {
                        span: 12
                    }
                }
            },
            userName: {
                type: 'string',
                title: '用户名称',
                ui: {
                    placeholder: '请输入用户名称',
                    grid: {
                        span: 12
                    }
                },
                minLength: 2,
                maxLength: 20
            },
            password: {
                type: 'string',
                title: '用户密码',
                ui: {
                    widget: 'password',
                    placeholder: '请输入用户密码',
                    grid: {
                        span: 12
                    }
                },
                minLength: 5,
                maxLength: 20
            },
            sex: {
                type: 'string',
                title: '用户性别',
                ui: {
                    widget: 'select',
                    placeholder: '请选择',
                    grid: {
                        span: 12
                    },
                    asyncData: () => injector.get(SystemDictService).getDicts('sys_user_sex')
                }
            },
            status: {
                type: 'string',
                title: '状态',
                ui: {
                    widget: 'radio',
                    placeholder: '请选择',
                    styleType: 'default',
                    buttonStyle: 'solid',
                    grid: {
                        span: 12
                    },
                    asyncData: () => injector.get(SystemDictService).getDicts('sys_normal_disable')
                },
                default: '0'
            },
            postIds: {
                type: 'string',
                title: '岗位',
                ui: {
                    widget: 'select',
                    placeholder: '请选择',
                    mode: 'multiple',
                    grid: {
                        span: 12
                    }
                },
                enum: []
            },
            roleIds: {
                type: 'string',
                title: '角色',
                ui: {
                    widget: 'select',
                    placeholder: '请选择',
                    mode: 'multiple',
                    grid: {
                        span: 12
                    }
                },
                enum: []
            },
            remark: {
                type: 'string',
                title: '备注',
                ui: {
                    widget: 'textarea',
                    placeholder: '请输入内容',
                    spanLabel: 4,
                    spanControl: 20,
                    autosize: { minRows: 2, maxRows: 6 },
                    grid: {
                        span: 24
                    }
                }
            }
        },
        required: ['userName', 'nickName', 'password'],
        ui: {
            spanLabel: 8,
            spanControl: 16,
            grid: {
                span: 24
            }
        }
    };
}

export function EDIT_USER_SCHEMA(injector: Injector): SFSchema {
    return {
        properties: {
            nickName: {
                type: 'string',
                title: '用户昵称',
                ui: {
                    placeholder: '请输入用户昵称',
                    grid: {
                        span: 12
                    }
                },
                maxLength: 30
            },
            deptId: {
                type: 'string',
                title: '归属部门',
                ui: {
                    widget: 'tree-select',
                    placeholder: '请选择归属部门',
                    virtualHeight: '300px',
                    grid: {
                        span: 12
                    },
                    asyncData: () => injector.get(SystemUserService).deptTreeSelectSF()
                }
            },
            phonenumber: {
                type: 'string',
                title: '手机号码',
                format: 'mobile',
                ui: {
                    placeholder: '请输入手机号码',
                    grid: {
                        span: 12
                    }
                },
                maxLength: 11
            },
            email: {
                type: 'string',
                title: '邮箱',
                format: 'email',
                ui: {
                    placeholder: '请输入邮箱',
                    grid: {
                        span: 12
                    }
                }
            },
            sex: {
                type: 'string',
                title: '用户性别',
                ui: {
                    widget: 'select',
                    placeholder: '请选择',
                    grid: {
                        span: 12
                    },
                    asyncData: () => injector.get(SystemDictService).getDicts('sys_user_sex')
                }
            },
            status: {
                type: 'string',
                title: '状态',
                ui: {
                    widget: 'radio',
                    placeholder: '请选择',
                    styleType: 'default',
                    buttonStyle: 'solid',
                    grid: {
                        span: 12
                    },
                    asyncData: () => injector.get(SystemDictService).getDicts('sys_normal_disable')
                },
                default: '0'
            },
            postIds: {
                type: 'string',
                title: '岗位',
                ui: {
                    widget: 'select',
                    placeholder: '请选择',
                    mode: 'multiple',
                    grid: {
                        span: 12
                    }
                },
                enum: []
            },
            roleIds: {
                type: 'string',
                title: '角色',
                ui: {
                    widget: 'select',
                    placeholder: '请选择',
                    mode: 'multiple',
                    grid: {
                        span: 12
                    }
                },
                enum: []
            },
            remark: {
                type: 'string',
                title: '备注',
                ui: {
                    widget: 'textarea',
                    placeholder: '请输入内容',
                    spanLabel: 4,
                    spanControl: 20,
                    autosize: { minRows: 2, maxRows: 6 },
                    grid: {
                        span: 24
                    }
                }
            }
        },
        required: ['nickName'],
        ui: {
            spanLabel: 8,
            spanControl: 16,
            grid: {
                span: 24
            }
        }
    };
}

export function RESET_USER_PASSWORD_SCHEMA(injector: Injector): SFSchema {
    return {
        properties: {
            password: {
                type: 'string',
                title: '重置密码',
                ui: {
                    widget: 'password',
                    placeholder: '请输入用户密码',
                    spanLabel: 6,
                    spanControl: 18,
                    grid: {
                        span: 24
                    }
                },
                minLength: 5,
                maxLength: 20
            }
        },
        required: ['password'],
        ui: {
            spanLabel: 8,
            spanControl: 16,
            grid: {
                span: 24
            }
        }
    };
}
