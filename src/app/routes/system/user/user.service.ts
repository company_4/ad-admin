import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SystemBasicService } from '@routes/system/_base';
import { UserForm, UserInfoVO } from '@types';
import { NzTreeNode } from 'ng-zorro-antd/tree';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SystemUserService extends SystemBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    /**
     * 用户状态修改
     * @param params
     */
    changeStatus(params: { userId: number | string; status: string }): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.user.changeStatus, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '用户状态修改', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 用户状态修改
     * @param params
     */
    resetUserPwd(params: { userId: string | number; password: string }): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.user.resetPwd, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '用户状态修改', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 新增用户
     * @param params
     */
    addUser(params: UserForm): Observable<ApiResToComponent<any>> {
        return this.client.post(this.apiUrl.user.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '新增用户', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 修改用户
     * @param params
     */
    updateUser(params: UserForm): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.user.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '修改用户', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 查询部门下拉树结构
     */
    deptTreeSelect(): Observable<ApiResToComponent<NzTreeNode[]>> {
        return this.client.get(this.apiUrl.user.deptTree).pipe(
            map((ev: ApiResponse) => {
                this.arrSer.visitTree(ev.data, (item, parent) => {
                    item.title = item.label;
                    item.key = item.id;
                    item.isLeaf = !item.children;
                });
                return this.doWithResponse(ev, { msg: '查询部门下拉树结构', color: 'grey' }, true, false);
            })
        );
    }
    /**
     * 查询部门下拉树结构
     */
    deptTreeSelectSF(): Observable<NzTreeNode[]> {
        return this.client.get(this.apiUrl.user.deptTree).pipe(
            map((ev: ApiResponse) => {
                this.arrSer.visitTree(ev.data, (item, parent) => {
                    item.title = item.label;
                    item.key = item.id;
                    item.isLeaf = !item.children;
                });
                return ev.data;
            })
        );
    }
    /**
     * 查询用户详细
     */
    getUser(userId: string = ''): Observable<ApiResToComponent<UserInfoVO>> {
        return this.client.get(this.apiUrl.user.getInfo + userId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询用户详细', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 删除用户
     */
    delUsers(userIds: string): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.user.delUsers + userIds).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除用户', color: 'grey' }, true, false);
            })
        );
    }
}
