import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SystemDictModule } from '@routes/system/dict/dict.module';
import { SharedModule } from '@shared';

import { SystemEditPostComponent } from './edit-post/edit-post.component';
import { SystemPostListComponent } from './post-list/post-list.component';

@NgModule({
    declarations: [SystemPostListComponent, SystemEditPostComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index',
                component: SystemPostListComponent
            }
        ]),
        SystemDictModule
    ]
})
export class SystemPostModule {}
