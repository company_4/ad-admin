import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { ApiResToComponent } from '@basic';
import { SFSchema } from '@delon/form';
import { BaseOfSystemForm } from '@routes/system/_base';
import { PostForm, PostVO } from '@types';
import { Observable } from 'rxjs';

import { SystemDictService } from '../../dict/dict.service';
import { SystemPostService } from '../post.service';

@Component({
    selector: 'app-system-edit-post',
    templateUrl: './edit-post.component.html',
    styleUrls: ['./edit-post.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemEditPostComponent extends BaseOfSystemForm<PostForm, PostVO> implements OnInit {
    protected makeParams(value: any): PostForm {
        let _temp = this.getValuesFormSf(value);
        if (this.type === 'edit') {
            return { ..._temp, postId: this.item.postId };
        }
        return _temp;
    }

    schema: SFSchema = {
        properties: {
            postName: {
                type: 'string',
                title: '岗位名称',
                ui: {
                    placeholder: '请输入岗位名称',
                    grid: {
                        span: 24
                    }
                }
            },
            postCode: {
                type: 'string',
                title: '岗位编码',
                ui: {
                    placeholder: '请输入编码名称',
                    grid: {
                        span: 24
                    }
                }
            },
            postSort: {
                type: 'number',
                title: '岗位顺序',
                ui: {
                    grid: {
                        span: 24
                    }
                },
                default: 0
            },
            status: {
                type: 'string',
                title: '岗位状态',
                ui: {
                    widget: 'radio',
                    styleType: 'default',
                    buttonStyle: 'solid',
                    grid: {
                        span: 24
                    },
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_normal_disable')
                },
                default: '0'
            },
            remark: {
                type: 'string',
                title: '备注',
                ui: {
                    widget: 'textarea',
                    placeholder: '请输入内容',
                    autosize: { minRows: 2, maxRows: 6 },
                    grid: {
                        span: 24
                    }
                }
            }
        },
        required: ['postName', 'postCode', 'postSort'],
        ui: {
            spanLabel: 6,
            spanControl: 16,
            grid: {
                span: 24
            }
        }
    };

    constructor(
        protected override injector: Injector,
        protected postSrv: SystemPostService
    ) {
        super(injector);
    }

    override ngOnInit() {
        if (this.item.postId) {
            this.postSrv.getPost(this.item.postId).subscribe(res => {
                this.item = res.data;
                this.cdRef.detectChanges();
            });
        }
    }

    sendToServer(value: any): Observable<ApiResToComponent<PostVO>> {
        if (this.item.postId) {
            return this.postSrv.updatePost(value);
        }
        return this.postSrv.addPost(value);
    }
}
