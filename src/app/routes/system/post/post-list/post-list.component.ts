import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { STColumn } from '@delon/abc/st';
import { SFSchema } from '@delon/form';
import { UrlOfSystemTable } from '@routes/system/_base';
import { PostVO } from '@types';

import { SystemDictService } from '../../dict/dict.service';
import { SystemEditPostComponent } from '../edit-post/edit-post.component';
import { SystemPostService } from '../post.service';

@Component({
    selector: 'app-system-post-list',
    templateUrl: './post-list.component.html',
    styleUrls: ['./post-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemPostListComponent extends UrlOfSystemTable implements OnInit {
    columns: STColumn[] = [
        {
            title: '编号',
            fixed: 'left',
            width: '50px',
            type: 'checkbox',
            className: 'text-center',
            exported: false
        },
        {
            title: '岗位编号',
            index: 'postId',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.postId }) }
        },
        {
            title: '岗位编码',
            index: 'postCode',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.postCode }) }
        },
        {
            title: '岗位名称',
            index: 'postName',
            className: 'text-center',
            width: '230px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.postName }) }
        },
        {
            title: '岗位排序',
            index: 'postSort',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.postSort }) }
        },
        {
            title: '状态',
            index: 'status',
            className: 'text-center',
            width: '130px',
            render: 'statusCustom'
        },
        {
            title: '创建时间',
            index: 'createTime',
            className: 'text-center',
            width: '200px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.createTime }) }
        },
        {
            title: '操作',
            width: '230px',
            className: 'text-center',
            buttons: [
                {
                    text: '修改',
                    iif: item => item.roleId !== 1,
                    // tooltip: `修改`,
                    acl: ['admin', 'system:post:edit'],
                    icon: 'edit',
                    click: item => this.editPost(item)
                },
                {
                    text: '删除',
                    iif: item => item.roleId !== 1,
                    // tooltip: '删除',
                    acl: ['admin', 'system:post:remove'],
                    icon: 'delete',
                    className: 'text-error',
                    click: item => this.delPost(item)
                }
            ]
        }
    ];
    searchSchema: SFSchema = {
        properties: {
            postCode: {
                type: 'string',
                title: '岗位编码',
                ui: {
                    placeholder: '请输入岗位编码'
                }
            },
            postName: {
                type: 'string',
                title: '岗位名称',
                ui: {
                    placeholder: '请输入岗位名称'
                }
            },
            status: {
                title: '状态',
                type: 'string',
                ui: {
                    placeholder: '角色状态',
                    widget: 'select',
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_normal_disable')
                }
            }
        }
    };
    url: string = this.apiUrl.post.list;

    constructor(
        protected override injector: Injector,
        private postSrv: SystemPostService
    ) {
        super(injector);
    }
    addPost() {
        this.modal
            .creatStaticModal(SystemEditPostComponent, { item: {} }, 'md', {
                nzTitle: '添加岗位'
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    editPost(item: PostVO) {
        this.modal
            .creatStaticModal(SystemEditPostComponent, { item, type: 'edit' }, 'md', {
                nzTitle: `修改岗位【${item.postName}】`
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    delPost(item: PostVO) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除岗位编号为 ${item.postId} 的数据项？`,
            nzOnOk: () =>
                this.postSrv.delPost(item.postId.toString()).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }
    delPostS(items: PostVO[]) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除岗位编号为 ${items.map(xx => xx.postId).join(',')} 的数据项？`,
            nzOnOk: () =>
                this.postSrv.delPost(items.map(xx => xx.postId).join(',')).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }
}
