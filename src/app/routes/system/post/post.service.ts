import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SystemBasicService } from '@routes/system/_base';
import { DictDataOption, PostForm, PostVO } from '@types';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SystemPostService extends SystemBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    /**
     * 新增岗位
     * @param params
     */
    addPost(params: PostForm): Observable<ApiResToComponent<any>> {
        return this.client.post(this.apiUrl.post.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '新增岗位', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 修改岗位
     * @param params
     */
    updatePost(params: PostForm): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.post.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '修改岗位', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 查询用户详细
     */
    getPost(postId: string | number): Observable<ApiResToComponent<PostVO>> {
        return this.client.get(this.apiUrl.post.getInfo + postId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询用户详细', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 删除岗位
     */
    delPost(postId: string | number): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.post.delPost + postId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除岗位', color: 'grey' }, true, false);
            })
        );
    }
}
