import { NgModule } from '@angular/core';
import { SystemNoticeListComponent } from './notice-list/notice-list.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared';
import { SystemDictModule } from '@routes/system/dict/dict.module';
import { SystemEditNoticeComponent } from './edit-notice/edit-notice.component';

@NgModule({
    declarations: [SystemNoticeListComponent, SystemEditNoticeComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index',
                component: SystemNoticeListComponent
            }
        ]),
        SystemDictModule
    ]
})
export class SystemNoticeModule {}
