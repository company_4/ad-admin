import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { ApiResToComponent } from '@basic';
import { SFSchema } from '@delon/form';
import { BaseOfSystemForm } from '@routes/system/_base';
import { NoticeForm, NoticeVO } from '@types';
import { Observable } from 'rxjs';

import { SystemDictService } from '../../dict/dict.service';
import { SystemNoticeService } from '../notice.service';

@Component({
    selector: 'app-system-edit-notice',
    templateUrl: './edit-notice.component.html',
    styleUrls: ['./edit-notice.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemEditNoticeComponent extends BaseOfSystemForm<NoticeForm, NoticeVO> implements OnInit {
    protected makeParams(value: any): NoticeForm {
        let _temp = this.getValuesFormSf(value);
        if (this.type === 'edit') {
            return { ..._temp, noticeId: this.item.noticeId };
        }
        return _temp;
    }

    schema: SFSchema = {
        properties: {
            noticeTitle: {
                type: 'string',
                title: '公告标题',
                ui: {
                    placeholder: '请输入公告标题',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    }
                }
            },
            noticeType: {
                type: 'string',
                title: '系统内置',
                ui: {
                    widget: 'select',
                    grid: {
                        span: 12
                    },
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_notice_type')
                }
            },
            status: {
                type: 'string',
                title: '状态',
                ui: {
                    widget: 'radio',
                    styleType: 'default',
                    buttonStyle: 'solid',
                    grid: {
                        span: 12
                    },
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_notice_status')
                },
                default: '0'
            },
            noticeContent: {
                type: 'string',
                title: '内容',
                ui: {
                    widget: 'editor',
                    placeholder: '请输入内容',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    }
                }
            }
        },
        required: ['noticeTitle', 'noticeType'],
        ui: {
            spanLabel: 8,
            spanControl: 16,
            grid: {
                span: 24
            }
        }
    };

    constructor(
        protected override injector: Injector,
        protected noticeSrv: SystemNoticeService
    ) {
        super(injector);
    }

    override ngOnInit() {
        if (this.item.noticeId) {
            this.noticeSrv.getNotice(this.item.noticeId).subscribe(res => {
                this.item = res.data;
                this.cdRef.detectChanges();
            });
        }
    }

    sendToServer(value: any): Observable<ApiResToComponent<NoticeVO>> {
        if (this.item.noticeId) {
            return this.noticeSrv.updateNotice(value);
        }
        return this.noticeSrv.addNotice(value);
    }
}
