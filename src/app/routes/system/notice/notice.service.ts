import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SystemBasicService } from '@routes/system/_base';
import { ConfigForm, NoticeVO } from '@types';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SystemNoticeService extends SystemBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    /**
     * 新增公告
     * @param params
     */
    addNotice(params: ConfigForm): Observable<ApiResToComponent<any>> {
        return this.client.post(this.apiUrl.notice.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '新增公告', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 修改公告
     * @param params
     */
    updateNotice(params: ConfigForm): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.notice.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '修改公告', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 查询公告详细
     */
    getNotice(noticeId: string | number): Observable<ApiResToComponent<NoticeVO>> {
        return this.client.get(this.apiUrl.notice.getInfo + noticeId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询公告详细', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 删除公告
     */
    delNotice(noticeId: string | number): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.notice.delNotice + noticeId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除公告', color: 'grey' }, true, false);
            })
        );
    }
}
