import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { STColumn } from '@delon/abc/st';
import { SFSchema } from '@delon/form';
import { UrlOfSystemTable } from '@routes/system/_base';
import { SystemDictService } from '@routes/system/dict/dict.service';
import { NoticeVO } from '@types';

import { SystemEditNoticeComponent } from '../edit-notice/edit-notice.component';
import { SystemNoticeService } from '../notice.service';

@Component({
    selector: 'app-system-notice-list',
    templateUrl: './notice-list.component.html',
    styleUrls: ['./notice-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemNoticeListComponent extends UrlOfSystemTable implements OnInit {
    columns: STColumn[] = [
        {
            title: '编号',
            fixed: 'left',
            width: '50px',
            type: 'checkbox',
            className: 'text-center',
            exported: false
        },
        {
            title: '序号',
            index: 'noticeId',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.noticeId }) }
        },
        {
            title: '公告标题',
            index: 'noticeTitle',
            className: 'text-center',
            width: '330px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.noticeTitle }) }
        },
        {
            title: '公告类型',
            index: 'noticeType',
            className: 'text-center',
            width: '130px',
            render: 'noticeTypeCustom'
        },
        {
            title: '状态',
            index: 'status',
            className: 'text-center',
            width: '130px',
            render: 'statusCustom'
        },
        {
            title: '创建者',
            index: 'createBy',
            className: 'text-center',
            width: '230px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.createBy }) }
        },
        {
            title: '创建时间',
            index: 'createTime',
            className: 'text-center',
            width: '200px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.createTime }) }
        },
        {
            title: '操作',
            width: '230px',
            className: 'text-center',
            buttons: [
                {
                    text: '修改',
                    // tooltip: `修改`,
                    acl: ['admin', 'system:notice:edit'],
                    icon: 'edit',
                    click: item => this.editNotice(item)
                },
                {
                    text: '删除',
                    // tooltip: '删除',
                    acl: ['admin', 'system:notice:remove'],
                    icon: 'delete',
                    className: 'text-error',
                    click: item => this.delNotice(item)
                }
            ]
        }
    ];
    searchSchema: SFSchema = {
        properties: {
            noticeTitle: {
                type: 'string',
                title: '公告标题',
                ui: {
                    placeholder: '请输入公告标题'
                }
            },
            createBy: {
                type: 'string',
                title: '操作人员',
                ui: {
                    placeholder: '请输入操作人员'
                }
            },
            noticeType: {
                title: '公告类型',
                type: 'string',
                ui: {
                    placeholder: '公告类型',
                    widget: 'select',
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_notice_type')
                }
            },
            range: {
                type: 'string',
                title: '创建时间',
                ui: {
                    widget: 'date',
                    mode: 'range',
                    placeholder: '选择时间段'
                }
            }
        }
    };
    url: string = this.apiUrl.notice.list;

    constructor(
        protected override injector: Injector,
        private noticeSrv: SystemNoticeService
    ) {
        super(injector);
    }

    addNotice() {
        this.modal
            .creatStaticModal(SystemEditNoticeComponent, { item: {} }, 'lg', {
                nzTitle: '添加公告'
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    editNotice(item: NoticeVO) {
        this.modal
            .creatStaticModal(SystemEditNoticeComponent, { item, type: 'edit' }, 'lg', {
                nzTitle: `修改公告【${item.noticeTitle}】`
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    delNotice(item: NoticeVO) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除公告编号为 ${item.noticeId} 的数据项？`,
            nzOnOk: () =>
                this.noticeSrv.delNotice(item.noticeId.toString()).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }
    delNoticeS(items: NoticeVO[]) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除公告编号为 ${items.map(xx => xx.noticeId).join(',')} 的数据项？`,
            nzOnOk: () =>
                this.noticeSrv.delNotice(items.map(xx => xx.noticeId).join(',')).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }
}
