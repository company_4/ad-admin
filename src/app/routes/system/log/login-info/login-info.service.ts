import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SystemBasicService } from '@routes/system/_base';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SystemLoginInfoService extends SystemBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    /**
     * 删除登录日志
     */
    delLoginInfo(infoId: string | number): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.loginInfo.delLoginInfo + infoId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除登录日志', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 清空登录日志
     */
    cleanLoginInfo(): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.loginInfo.cleanLoginInfo).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '清空登录日志', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 解锁用户登录状态
     */
    unlockLoginInfo(userName: string): Observable<ApiResToComponent<null>> {
        return this.client.get(this.apiUrl.loginInfo.unlockLoginInfo + userName).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '解锁用户登录状态', color: 'grey' }, true, false);
            })
        );
    }
}
