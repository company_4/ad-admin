import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SystemDictModule } from '@routes/system/dict/dict.module';
import { SharedModule } from '@shared';

import { SystemLoginInfoComponent } from './login-info.component';

@NgModule({
    declarations: [SystemLoginInfoComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index',
                component: SystemLoginInfoComponent
            }
        ]),
        SystemDictModule
    ]
})
export class SystemLoginInfoModule {}
