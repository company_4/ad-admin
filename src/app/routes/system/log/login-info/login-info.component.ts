import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { STColumn } from '@delon/abc/st';
import { SFSchema } from '@delon/form';
import { UrlOfSystemTable } from '@routes/system/_base';
import { SystemDictService } from '@routes/system/dict/dict.service';
import { SystemLoginInfoService } from '@routes/system/log/login-info/login-info.service';
import { LoginInfoVO } from '@types';

@Component({
    selector: 'app-System-login-info',
    templateUrl: './login-info.component.html',
    styleUrls: ['./login-info.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemLoginInfoComponent extends UrlOfSystemTable implements OnInit {
    columns: STColumn[] = [
        {
            title: '编号',
            fixed: 'left',
            width: '50px',
            type: 'checkbox',
            className: 'text-center',
            exported: false
        },
        {
            title: '访问编号',
            index: 'infoId',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.infoId }) }
        },
        {
            title: '用户名称',
            index: 'userName',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.userName }) }
        },
        {
            title: '登录地址',
            index: 'ipaddr',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.ipaddr }) }
        },
        {
            title: '登录地点',
            index: 'loginLocation',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.loginLocation }) }
        },
        {
            title: '浏览器',
            index: 'browser',
            className: 'text-center',
            width: '100px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.browser }) }
        },
        {
            title: '操作系统',
            index: 'os',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.os }) }
        },
        {
            title: '登录状态',
            index: 'status',
            className: 'text-center',
            width: '80px',
            render: 'statusCustom'
        },
        {
            title: '描述',
            index: 'msg',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.msg }) }
        },
        {
            title: '访问时间',
            index: 'loginTime',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.loginTime }) }
        }
    ];
    searchSchema: SFSchema = {
        properties: {
            ipaddr: {
                type: 'string',
                title: '登录地址',
                ui: {
                    placeholder: '请输入登录地址'
                }
            },
            userName: {
                type: 'string',
                title: '用户名称',
                ui: {
                    placeholder: '请输入用户名称'
                }
            },
            status: {
                type: 'string',
                title: '状态',
                ui: {
                    placeholder: '登录状态',
                    widget: 'select',
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_common_status')
                }
            },
            range: {
                type: 'string',
                title: '登录时间',
                ui: {
                    widget: 'date',
                    mode: 'range',
                    placeholder: '选择时间段'
                }
            }
        }
    };
    url: string = this.apiUrl.loginInfo.list;

    constructor(
        protected override injector: Injector,
        private loginInfoSrv: SystemLoginInfoService
    ) {
        super(injector);
    }
    delLoginLogS(items: LoginInfoVO[]) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除访问编号为 ${items.map(xx => xx.infoId).join(',')} 的数据项？`,
            nzOnOk: () =>
                this.loginInfoSrv.delLoginInfo(items.map(xx => xx.infoId).join(',')).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }
    clearLoginLogS() {
        this.modalSrv.confirm({
            nzTitle: '清空提示',
            nzContent: `是否确认清空所有登录日志数据项？`,
            nzOnOk: () =>
                this.loginInfoSrv.cleanLoginInfo().subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('清空成功');
                    }
                    this.st.reload();
                })
        });
    }
    unlockUser(item: LoginInfoVO) {
        this.modalSrv.confirm({
            nzTitle: '解锁提示',
            nzContent: `是否确认解锁用户 ${item.userName} 的数据项？`,
            nzOnOk: () =>
                this.loginInfoSrv.unlockLoginInfo(item.userName).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('解锁成功');
                    }
                    this.st.reload();
                })
        });
    }
}
