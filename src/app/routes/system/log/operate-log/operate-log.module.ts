import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SystemDictModule } from '@routes/system/dict/dict.module';
import { SharedModule } from '@shared';

import { SystemOperateDetailComponent } from './operate-detail/operate-detail.component';
import { SystemOperateLogComponent } from './operate-log.component';

@NgModule({
    declarations: [SystemOperateLogComponent, SystemOperateDetailComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index',
                component: SystemOperateLogComponent
            }
        ]),
        SystemDictModule
    ]
})
export class SystemOperateLogModule {}
