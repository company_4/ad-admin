import { Component, Input } from '@angular/core';
import { OperLogVO } from '@types';

@Component({
    selector: 'app-system-operate-detail',
    templateUrl: './operate-detail.component.html',
    styleUrls: ['./operate-detail.component.less']
})
export class SystemOperateDetailComponent {
    @Input()
    item!: OperLogVO;

    statusEnum = {
        '0': '正常',
        '1': '失败'
    };
}
