import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { STColumn } from '@delon/abc/st';
import { SFSchema } from '@delon/form';
import { UrlOfSystemTable } from '@routes/system/_base';
import { SystemDictService } from '@routes/system/dict/dict.service';
import { SystemOperateDetailComponent } from '@routes/system/log/operate-log/operate-detail/operate-detail.component';
import { OperLogVO } from '@types';

import { SystemOperateLogService } from './operate-log.service';

@Component({
    selector: 'app-system-operate-log',
    templateUrl: './operate-log.component.html',
    styleUrls: ['./operate-log.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemOperateLogComponent extends UrlOfSystemTable implements OnInit {
    columns: STColumn[] = [
        {
            title: '编号',
            fixed: 'left',
            width: '50px',
            type: 'checkbox',
            className: 'text-center',
            exported: false
        },
        {
            title: '日志编号',
            index: 'operId',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.operId }) }
        },
        {
            title: '系统模块',
            index: 'title',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.title }) }
        },
        {
            title: '操作类型',
            index: 'businessType',
            className: 'text-center',
            width: '130px',
            render: 'businessTypeCustom'
        },
        {
            title: '请求方式',
            index: 'requestMethod',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.requestMethod }) }
        },
        {
            title: '操作人员',
            index: 'operName',
            className: 'text-center',
            width: '100px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.operName }) }
        },
        {
            title: '主机',
            index: 'operIp',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.operIp }) }
        },
        {
            title: '操作状态',
            index: 'status',
            className: 'text-center',
            width: '80px',
            render: 'statusCustom'
        },
        {
            title: '消耗时间',
            index: 'costTime',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: `${record.costTime ?? 0}毫秒` }) }
        },
        {
            title: '操作日期',
            index: 'operTime',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.operTime }) }
        },
        {
            title: '操作',
            width: '80px',
            className: 'text-center',
            buttons: [
                {
                    text: '详细',
                    iif: item => item.roleId !== 1,
                    // tooltip: `修改`,
                    acl: ['admin', 'system:operlog:query'],
                    icon: 'eye',
                    click: item => this.seeDetail(item)
                }
            ]
        }
    ];
    searchSchema: SFSchema = {
        properties: {
            title: {
                type: 'string',
                title: '系统模块',
                ui: {
                    placeholder: '请输入系统模块'
                }
            },
            operName: {
                type: 'string',
                title: '操作人员',
                ui: {
                    placeholder: '请输入操作人员'
                }
            },
            businessType: {
                type: 'string',
                title: '操作类型',
                ui: {
                    placeholder: '操作类型',
                    widget: 'select',
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_oper_type')
                }
            },
            status: {
                type: 'string',
                title: '状态',
                ui: {
                    placeholder: '操作状态',
                    widget: 'select',
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_common_status')
                }
            },
            range: {
                type: 'string',
                title: '操作时间',
                ui: {
                    widget: 'date',
                    mode: 'range',
                    placeholder: '选择时间段'
                }
            }
        }
    };
    url: string = this.apiUrl.operateLog.list;

    constructor(
        protected override injector: Injector,
        private operateSrv: SystemOperateLogService
    ) {
        super(injector);
    }
    delLoginLogS(items: OperLogVO[]) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除日志编号为 ${items.map(xx => xx.operId).join(',')} 的数据项？`,
            nzOnOk: () =>
                this.operateSrv.delOperateLog(items.map(xx => xx.operId).join(',')).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }
    clearLoginLogS() {
        this.modalSrv.confirm({
            nzTitle: '清空提示',
            nzContent: `是否确认清空所有操作日志数据项？`,
            nzOnOk: () =>
                this.operateSrv.cleanOperateLog().subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('清空成功');
                    }
                    this.st.reload();
                })
        });
    }

    seeDetail(item: any) {
        this.modal
            .creatStaticModal(SystemOperateDetailComponent, { item }, 'md', {
                nzTitle: `操作日志详细`
            })
            .subscribe(res => {});
    }
}
