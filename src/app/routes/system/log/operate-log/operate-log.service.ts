import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SystemBasicService } from '@routes/system/_base';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SystemOperateLogService extends SystemBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    /**
     * 删除操作日志
     */
    delOperateLog(operId: string | number): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.operateLog.delOperateLog + operId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除操作日志', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 清空操作日志
     */
    cleanOperateLog(): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.operateLog.cleanOperateLog).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '清空操作日志', color: 'grey' }, true, false);
            })
        );
    }
}
