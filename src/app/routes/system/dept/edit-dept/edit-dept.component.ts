import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { ApiResToComponent } from '@basic';
import { SFSchema } from '@delon/form';
import { BaseOfSystemForm } from '@routes/system/_base';
import { DeptForm, DeptVO } from '@types';
import { Observable } from 'rxjs';

import { SystemDictService } from '../../dict/dict.service';
import { SystemDeptService } from '../dept.service';

@Component({
    selector: 'app-system-edit-dept',
    templateUrl: './edit-dept.component.html',
    styleUrls: ['./edit-dept.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemEditDeptComponent extends BaseOfSystemForm<DeptForm, DeptVO> implements OnInit {
    protected makeParams(value: any): DeptForm {
        return value;
    }

    schema: SFSchema = {
        properties: {
            parentId: {
                type: 'string',
                title: '上级部门',
                ui: {
                    widget: 'tree-select',
                    placeholder: '选择上级部门',
                    spanLabel: 4,
                    virtualHeight: '300px',
                    spanControl: 8,
                    grid: {
                        span: 24
                    },
                    asyncData: () => this.deptSrv.deptTreeSelectSF()
                },
                default: 0
            },
            deptName: {
                type: 'string',
                title: '部门名称',
                ui: {
                    placeholder: '请输入部门名称',
                    grid: {
                        span: 12
                    }
                }
            },
            orderNum: {
                type: 'number',
                title: '显示排序',
                ui: {
                    grid: {
                        span: 12
                    }
                },
                default: 0
            },
            leader: {
                type: 'string',
                title: '负责人',
                ui: {
                    placeholder: '请输入负责人',
                    grid: {
                        span: 12
                    }
                }
            },
            phone: {
                type: 'string',
                title: '联系电话',
                format: 'mobile',
                ui: {
                    placeholder: '请输入联系电话',
                    grid: {
                        span: 12
                    }
                },
                maxLength: 11
            },
            email: {
                type: 'string',
                title: '邮箱',
                format: 'email',
                ui: {
                    placeholder: '请输入邮箱',
                    grid: {
                        span: 12
                    }
                }
            },
            status: {
                type: 'string',
                title: '部门状态',
                ui: {
                    widget: 'radio',
                    placeholder: '请选择',
                    styleType: 'default',
                    buttonStyle: 'solid',
                    grid: {
                        span: 12
                    },
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_normal_disable')
                },
                default: '0'
            }
        },
        required: ['parentId', 'orderNum', 'deptName'],
        ui: {
            spanLabel: 8,
            spanControl: 16,
            grid: {
                span: 24
            }
        }
    };

    sendToServer(value: any): Observable<ApiResToComponent<DeptVO>> {
        if (this.item.deptId) {
            return this.deptSrv.updateDept(value);
        }
        return this.deptSrv.addDept(value);
    }

    constructor(
        protected override injector: Injector,
        protected deptSrv: SystemDeptService
    ) {
        super(injector);
    }

    override ngOnInit() {
        if (this.item.deptId) {
            this.deptSrv.getDept(this.item.deptId).subscribe(res => {
                this.item = res.data;
                this.cdRef.detectChanges();
            });
        }
    }
}
