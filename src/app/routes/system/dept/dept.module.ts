import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SystemDictModule } from '@routes/system/dict/dict.module';
import { SharedModule } from '@shared';
import { NzDividerModule } from 'ng-zorro-antd/divider';

import { SystemDeptTreeComponent } from './dept-tree/dept-tree.component';
import { SystemEditDeptComponent } from './edit-dept/edit-dept.component';

@NgModule({
    declarations: [SystemDeptTreeComponent, SystemEditDeptComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index',
                component: SystemDeptTreeComponent
            }
        ]),
        NzDividerModule,
        SystemDictModule
    ]
})
export class SystemDeptModule {}
