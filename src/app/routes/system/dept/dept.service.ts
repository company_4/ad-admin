import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SFSchemaEnumType } from '@delon/form';
import { SystemBasicService } from '@routes/system/_base';
import { DeptForm, DeptVO } from '@types';
import { NzTreeNode } from 'ng-zorro-antd/tree';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SystemDeptService extends SystemBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    deptTreeSelectSF(): Observable<SFSchemaEnumType[]> {
        return this.client.get(this.apiUrl.dept.list).pipe(
            map((ev: ApiResponse) => {
                const tree = this.arrSer.arrToTree<NzTreeNode>(ev.data as NzTreeNode[], {
                    idMapName: 'deptId',
                    parentIdMapName: 'parentId',
                    cb: (item: any) => {
                        item.key = item.deptId;
                        item.title = item.deptName;
                    }
                });
                this.arrSer.visitTree(tree, item => {
                    if (item.children.length === 0) {
                        item.isLeaf = true;
                    }
                });
                return tree;
            })
        );
    }
    /**
     * 查询部门列表
     */
    listDept(params: any): Observable<ApiResToComponent<DeptVO[]>> {
        return this.client.get(this.apiUrl.dept.list, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询部门列表', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 新增部门
     * @param params
     */
    addDept(params: DeptForm): Observable<ApiResToComponent<any>> {
        return this.client.post(this.apiUrl.dept.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '新增部门', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 修改部门
     * @param params
     */
    updateDept(params: DeptForm): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.dept.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '修改部门', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 查询部门详细
     */
    getDept(deptId: string | number): Observable<ApiResToComponent<DeptVO>> {
        return this.client.get(this.apiUrl.dept.getInfo + deptId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询部门详细', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 删除部门
     */
    delDept(deptId: string | number): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.dept.delDept + deptId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除部门', color: 'grey' }, true, false);
            })
        );
    }
}
