import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SFSchemaEnumType } from '@delon/form';
import { SystemBasicService } from '@routes/system/_base';
import { MenuForm, MenuVO, RoleMenuTree } from '@types';
import { NzTreeNode } from 'ng-zorro-antd/tree';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SystemMenuService extends SystemBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    /**
     * 查询部门下拉树结构
     */
    treeSelectSF(): Observable<NzTreeNode[]> {
        return this.client.get(this.apiUrl.menu.treeSelect).pipe(
            map((ev: ApiResponse) => {
                this.arrSer.visitTree(ev.data, (item, parent) => {
                    item.title = item.label;
                    item.key = item.id;
                    item.isLeaf = !item.children;
                });
                return ev.data;
            })
        );
    }

    menuTreeSelectSF(): Observable<SFSchemaEnumType[]> {
        return this.client.get(this.apiUrl.menu.list).pipe(
            map((ev: ApiResponse) => {
                const tree = this.arrSer.arrToTree<NzTreeNode>(ev.data as NzTreeNode[], {
                    idMapName: 'menuId',
                    parentIdMapName: 'parentId',
                    cb: (item: any) => {
                        item.key = item.menuId;
                        item.title = item.menuName;
                    }
                });
                this.arrSer.visitTree(tree, item => {
                    if (item.children.length === 0) {
                        item.isLeaf = true;
                    }
                });
                return [{ key: 0, title: '主类目', children: tree }];
            })
        );
    }

    /**
     * 根据角色ID查询菜单下拉树结构
     */
    roleMenuTreeSelect(roleId: string = ''): Observable<ApiResToComponent<RoleMenuTree>> {
        return this.client.get(this.apiUrl.menu.roleMenuTreeselect + roleId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '根据角色ID查询菜单下拉树结构', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 查询菜单列表
     */
    listMenu(params: any): Observable<ApiResToComponent<MenuVO[]>> {
        return this.client.get(this.apiUrl.menu.list, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询菜单列表', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 新增菜单
     * @param params
     */
    addMenu(params: MenuForm): Observable<ApiResToComponent<any>> {
        return this.client.post(this.apiUrl.menu.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '新增菜单', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 修改菜单
     * @param params
     */
    updateMenu(params: MenuForm): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.menu.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '修改菜单', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 查询用户详细
     */
    getMenu(menuId: string | number): Observable<ApiResToComponent<MenuVO>> {
        return this.client.get(this.apiUrl.menu.getInfo + menuId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询用户详细', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 删除菜单
     */
    delMenu(menuId: string | number): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.menu.delMenu + menuId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除菜单', color: 'grey' }, true, false);
            })
        );
    }
}
