import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SystemDictModule } from '@routes/system/dict/dict.module';
import { SharedModule } from '@shared';
import { NzDividerModule } from 'ng-zorro-antd/divider';

import { SystemMenuTreeComponent } from './menu-tree/menu-tree.component';
import { SystemEditMenuComponent } from './edit-menu/edit-menu.component';

@NgModule({
    declarations: [SystemMenuTreeComponent, SystemEditMenuComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index',
                component: SystemMenuTreeComponent
            }
        ]),
        SystemDictModule,
        NzDividerModule
    ]
})
export class SystemMenuModule {}
