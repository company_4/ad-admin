import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { SFSchema } from '@delon/form';
import { ArrayService } from '@delon/util';
import { BaseOfSystemComponent } from '@routes/system/_base';
import { MenuVO } from '@types';

import { SystemDictService } from '../../dict/dict.service';
import { SystemEditMenuComponent } from '../edit-menu/edit-menu.component';
import { SystemMenuService } from '../menu.service';

export interface TreeNodeInterface extends MenuVO {
    key: string;
    perms: string;
    level?: number;
    expand?: boolean;
    children: TreeNodeInterface[];
    parent: TreeNodeInterface;
}

@Component({
    selector: 'app-system-menu-tree',
    templateUrl: './menu-tree.component.html',
    styleUrls: ['./menu-tree.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemMenuTreeComponent extends BaseOfSystemComponent {
    searchSchema: SFSchema = {
        properties: {
            menuName: {
                title: '菜单名称',
                type: 'string',
                ui: {
                    placeholder: '请输入菜单名称'
                }
            },
            status: {
                title: '状态',
                type: 'string',
                ui: {
                    placeholder: '请选择状态',
                    widget: 'select',
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_normal_disable')
                }
            }
        }
    };

    params: any = {};

    tableTreeExpand: boolean = false;

    listOfMapData: TreeNodeInterface[] = [];
    mapOfExpandedData: { [key: string]: TreeNodeInterface[] } = {};

    collapse(array: TreeNodeInterface[], data: TreeNodeInterface, $event: boolean): void {
        if (!$event) {
            if (data.children) {
                data.children.forEach(d => {
                    const target = array.find(a => a.key === d.key)!;
                    target.expand = false;
                    this.collapse(array, target, false);
                });
            } else {
                return;
            }
        }
    }

    convertTreeToList(root: TreeNodeInterface): TreeNodeInterface[] {
        const stack: TreeNodeInterface[] = [];
        const array: TreeNodeInterface[] = [];
        const hashMap = {};
        stack.push({ ...root, level: 0, expand: this.tableTreeExpand });

        while (stack.length !== 0) {
            const node = stack.pop()!;
            this.visitNode(node, hashMap, array);
            if (node.children) {
                for (let i = node.children.length - 1; i >= 0; i--) {
                    stack.push({ ...node.children[i], level: node.level! + 1, expand: this.tableTreeExpand, parent: node });
                }
            }
        }

        return array;
    }

    visitNode(node: TreeNodeInterface, hashMap: { [key: string]: boolean }, array: TreeNodeInterface[]): void {
        if (!hashMap[node.key]) {
            hashMap[node.key] = true;
            array.push(node);
        }
    }

    constructor(
        protected override injector: Injector,
        protected menuSrv: SystemMenuService,
        protected arrSrv: ArrayService
    ) {
        super(injector);
    }

    override ngOnInit(): void {
        this.listMenu();
    }

    expandTableAll() {
        this.tableTreeExpand = !this.tableTreeExpand;
        this.listOfMapData.forEach(item => {
            this.mapOfExpandedData[item.key] = this.convertTreeToList(item);
        });
        this.cdRef.detectChanges();
    }

    listMenu(params: any = {}) {
        this.menuSrv.listMenu(params).subscribe(res => {
            this.listOfMapData = this.arrSrv.arrToTree<TreeNodeInterface>((res.data ?? []) as TreeNodeInterface[], {
                idMapName: 'menuId',
                parentIdMapName: 'parentId',
                cb: (item: any) => {
                    item.key = item.menuId;
                }
            });
            this.listOfMapData.forEach(item => {
                this.mapOfExpandedData[item.key] = this.convertTreeToList(item);
            });
            this.cdRef.detectChanges();
        });
    }

    searchDataGet(params: any) {
        this.listMenu(params);
    }
    reSetDataGet(params: any) {
        this.listMenu();
    }

    addMenu() {
        this.modal
            .creatStaticModal(SystemEditMenuComponent, { item: {} }, 'lg', {
                nzTitle: '添加菜单'
            })
            .subscribe(res => {
                if (res) {
                    this.listMenu();
                }
            });
    }

    editMenu(item: MenuVO) {
        this.modal
            .creatStaticModal(SystemEditMenuComponent, { item: item }, 'lg', {
                nzTitle: '编辑菜单'
            })
            .subscribe(res => {
                if (res) {
                    this.listMenu();
                }
            });
    }

    addSubMenu(item: MenuVO) {
        this.modal
            .creatStaticModal(SystemEditMenuComponent, { item: { parentId: item.menuId } }, 'lg', {
                nzTitle: '新增菜单'
            })
            .subscribe(res => {
                if (res) {
                    this.listMenu();
                }
            });
    }

    delMenu(item: MenuVO) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除名称为 ${item.menuName} 的数据项？`,
            nzOnOk: () =>
                this.menuSrv.delMenu(item.menuId.toString()).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.listMenu();
                })
        });
    }
}
