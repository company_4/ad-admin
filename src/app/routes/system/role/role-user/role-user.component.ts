import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { STColumn } from '@delon/abc/st';
import { SFSchema } from '@delon/form';
import { UrlOfSystemTable } from '@routes/system/_base';
import { RoleVO, UserVO } from '@types';
import { NzModalRef } from 'ng-zorro-antd/modal';

import { SystemRoleService } from '../role.service';
import { SystemRoleSelectUserComponent } from '../select-user/select-user.component';

@Component({
    selector: 'app-system-role-user',
    templateUrl: './role-user.component.html',
    styleUrls: ['./role-user.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemRoleUserComponent extends UrlOfSystemTable implements OnInit {
    @Input()
    item!: RoleVO;

    columns: STColumn[] = [
        {
            title: '编号',
            fixed: 'left',
            width: '50px',
            type: 'checkbox',
            className: 'text-center',
            exported: false
        },
        {
            title: '用户编号',
            index: 'userId',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.userId }) }
        },
        {
            title: '用户名称',
            index: 'userName',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.userName }) }
        },
        {
            title: '用户昵称',
            index: 'nickName',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.nickName }) }
        },
        {
            title: '手机号码',
            index: 'phonenumber',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.phonenumber }) }
        },
        {
            title: '状态',
            index: 'status',
            className: 'text-center',
            width: '130px',
            render: 'statusCustom'
        },
        {
            title: '创建时间',
            index: 'createTime',
            className: 'text-center',
            width: '200px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.createTime }) }
        },
        {
            title: '操作',
            className: 'text-center',
            width: '120px',
            fixed: 'right',
            buttons: [
                {
                    text: `取消授权`,
                    acl: ['admin', 'system:role:remove'],
                    icon: 'close-circle',
                    click: item => this.cancelUser(item)
                }
            ]
        }
    ];
    searchSchema: SFSchema = {
        properties: {
            userName: {
                title: '用户名称',
                type: 'string',
                ui: {
                    placeholder: '请输入用户名称'
                }
            },
            phonenumber: {
                title: '手机号码',
                type: 'string',
                ui: {
                    placeholder: '请输入手机号码'
                }
            }
        }
    };
    url: string = this.apiUrl.role.allocatedList;

    modalOpened: boolean = false;

    override ngOnInit() {
        this.url = `${this.apiUrl.role.allocatedList}?roleId=${this.item.roleId}`;
        this.modalRef.afterOpen.subscribe(res => {
            this.modalOpened = true;
            this.cdRef.detectChanges();
        });
    }

    constructor(
        protected override injector: Injector,
        protected modalRef: NzModalRef,
        protected roleSrv: SystemRoleService
    ) {
        super(injector);
    }

    addUsers() {
        this.modal
            .creatStaticModal(SystemRoleSelectUserComponent, { item: this.item }, 'lg', {
                nzTitle: `添加用户到角色${this.item.roleName}`
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }

    cancelUsers(items: UserVO[]) {
        this.modal.confirm({
            nzTitle: '提示',
            nzContent: `是否取消选中用户授权数据项？`,
            nzOnOk: () =>
                this.roleSrv
                    .authUserCancelAll({ userIds: items.map(it => it.userId).join(','), roleId: this.item.roleId })
                    .subscribe(res => {
                        this.st.reload();
                        if (res.isComplete) {
                            this.msg.success('取消授权成功');
                        }
                    })
        });
    }
    cancelUser(item: UserVO) {
        this.modal.confirm({
            nzTitle: '提示',
            nzContent: `确认要取消该用户"${item.userName}"角色吗？`,
            nzOnOk: () =>
                this.roleSrv.authUserCancel({ userId: item.userId, roleId: this.item.roleId }).subscribe(res => {
                    this.st.reload();
                    if (res.isComplete) {
                        this.msg.success('取消授权成功');
                    }
                })
        });
    }
}
