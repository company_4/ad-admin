import { Component, Injector, Input, OnInit } from '@angular/core';
import { STColumn } from '@delon/abc/st';
import { SFSchema } from '@delon/form';
import { UrlOfSystemTable } from '@routes/system/_base';
import { RoleVO } from '@types';
import { NzModalRef } from 'ng-zorro-antd/modal';

import { SystemRoleService } from '../role.service';

@Component({
    selector: 'app-role-select-user',
    templateUrl: './select-user.component.html',
    styleUrls: ['./select-user.component.less']
})
export class SystemRoleSelectUserComponent extends UrlOfSystemTable implements OnInit {
    @Input()
    item!: RoleVO;

    columns: STColumn[] = [
        {
            title: '编号',
            fixed: 'left',
            width: '50px',
            type: 'checkbox',
            className: 'text-center',
            exported: false
        },
        {
            title: '用户编号',
            index: 'userId',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.userId }) }
        },
        {
            title: '用户名称',
            index: 'userName',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.userName }) }
        },
        {
            title: '用户昵称',
            index: 'nickName',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.nickName }) }
        },
        {
            title: '手机号码',
            index: 'phonenumber',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.phonenumber }) }
        },
        {
            title: '状态',
            index: 'status',
            className: 'text-center',
            width: '130px',
            render: 'statusCustom'
        },
        {
            title: '创建时间',
            index: 'createTime',
            className: 'text-center',
            width: '200px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.createTime }) }
        }
    ];
    searchSchema: SFSchema = {
        properties: {
            userName: {
                title: '用户名称',
                type: 'string',
                ui: {
                    placeholder: '请输入用户名称'
                }
            },
            phonenumber: {
                title: '手机号码',
                type: 'string',
                ui: {
                    placeholder: '请输入手机号码'
                }
            }
        }
    };
    url: string = this.apiUrl.role.allocatedList;

    modalOpened: boolean = false;
    submitForm() {
        this.roleSrv
            .selectAll({ userIds: this.multipleCheckedItems.map((it: any) => it.userId).join(','), roleId: this.item.roleId })
            .subscribe(res => {
                if (res.isComplete) {
                    this.msg.success('分配成功');
                }
                this.modalRef.destroy(true);
            });
    }

    override ngOnInit() {
        this.url = `${this.apiUrl.role.unallocatedList}?roleId=${this.item.roleId}`;
        this.modalRef.afterOpen.subscribe(res => {
            this.modalOpened = true;
            this.cdRef.detectChanges();
        });
    }

    constructor(
        protected override injector: Injector,
        protected modalRef: NzModalRef,
        protected roleSrv: SystemRoleService
    ) {
        super(injector);
    }
}
