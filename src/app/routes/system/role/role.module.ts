import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SystemDictModule } from '@routes/system/dict/dict.module';
import { SharedModule } from '@shared';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzTreeModule } from 'ng-zorro-antd/tree';

import { SystemRoleDataScopeComponent } from './data-scope/data-scope.component';
import { SystemEditRoleComponent } from './edit-role/edit-role.component';
import { SystemRoleListComponent } from './role-list/role-list.component';
import { SystemRoleUserComponent } from './role-user/role-user.component';
import { SystemRoleSelectUserComponent } from './select-user/select-user.component';

@NgModule({
    declarations: [
        SystemRoleListComponent,
        SystemEditRoleComponent,
        SystemRoleDataScopeComponent,
        SystemRoleUserComponent,
        SystemRoleSelectUserComponent
    ],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index',
                component: SystemRoleListComponent
            }
        ]),
        NzPopconfirmModule,
        NzTreeModule,
        NzCheckboxModule,
        NzInputNumberModule,
        NzRadioModule,
        NzSelectModule,
        NzTagModule,
        SystemDictModule
    ]
})
export class SystemRoleModule {}
