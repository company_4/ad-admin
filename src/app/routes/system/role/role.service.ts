import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SystemBasicService } from '@routes/system/_base';
import { RoleDeptTree, RoleForm, RoleVO } from '@types';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SystemRoleService extends SystemBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    /**
     * 角色状态修改
     * @param params
     */
    changeRoleStatus(params: { roleId: number | string; status: string }): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.role.changeStatus, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '角色状态修改', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 新增角色
     * @param params
     */
    addRole(params: RoleForm): Observable<ApiResToComponent<any>> {
        return this.client.post(this.apiUrl.role.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '新增角色', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 修改角色
     * @param params
     */
    updateRole(params: RoleForm): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.role.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '修改角色', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 删除用户
     */
    delRoles(roleIds: string): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.role.delRoles + roleIds).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除用户', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 查询角色详细
     */
    getRole(roleId: string = ''): Observable<ApiResToComponent<RoleVO>> {
        return this.client.get(this.apiUrl.role.getInfo + roleId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询角色详细', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 根据角色ID查询部门树结构
     */
    deptTreeSelect(roleId: string = ''): Observable<ApiResToComponent<RoleDeptTree>> {
        return this.client.get(this.apiUrl.role.deptTreeSelect + roleId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '根据角色ID查询部门树结构', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 角色数据权限
     * @param params
     */
    dataScope(params: RoleForm): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.role.dataScope, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '角色数据权限', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 取消用户授权角色
     * @param params
     */
    authUserCancel(params: { userId: string | number; roleId: string | number }): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.role.cancelUser, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '取消用户授权角色', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 批量取消用户授权角色
     * @param params
     */
    authUserCancelAll(params: { roleId: string | number; userIds: string }): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.role.authUserCancelAll, {}, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '批量取消用户授权角色', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 授权用户选择
     * @param params
     */
    selectAll(params: { roleId: string | number; userIds: string }): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.role.selectAll, {}, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '授权用户选择', color: 'grey' }, true, false);
            })
        );
    }
}
