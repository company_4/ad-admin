import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SystemBasicService } from '@routes/system/_base';
import { ConfigForm, NoticeVO } from '@types';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SystemOssService extends SystemBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    /**
     * 删除OSS对象存储
     */
    delOss(noticeId: string | number): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.resource.delOss + noticeId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除OSS对象存储', color: 'grey' }, true, false);
            })
        );
    }
}
