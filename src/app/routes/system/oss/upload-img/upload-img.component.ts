import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { FileSaveService } from '@basic';
import { deepCopy } from '@delon/util';
import { BaseOfSystemComponent } from '@routes/system/_base';
import { NzImageService } from 'ng-zorro-antd/image';
import { NzUploadChangeParam, NzUploadFile } from 'ng-zorro-antd/upload';
import { map } from 'rxjs';

import { SystemOssService } from '../oss.service';

@Component({
    selector: 'app-upload-img',
    templateUrl: './upload-img.component.html',
    styleUrls: ['./upload-img.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OssUploadImgComponent extends BaseOfSystemComponent {
    actionUrl = this.apiUrl.resource.upload;

    /**
     * 文件上传大小
     */
    nzSize: number = 5000;
    /**
     * 文件上传数量
     */
    nzLimit: number = 2;
    /**
     * 文件上传类型
     */
    nzAccept: string = '.doc,.xls,.ppt,.txt,.pdf';

    fileList: NzUploadFile[] = [];

    constructor(
        protected override injector: Injector,
        private ossSrv: SystemOssService,
        private fileSrv: FileSaveService,
        private nzImageService: NzImageService
    ) {
        super(injector);
    }

    handleChange(info: NzUploadChangeParam): void {
        console.log(deepCopy(info));
        if (info.file.status !== 'uploading') {
            console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
            this.msg.success(`${info.file.name} 文件上传成功`);
        } else if (info.file.status === 'error') {
            this.msg.error(`${info.file.name} 文件上传失败`);
        }
    }

    nzRemove = (file: NzUploadFile) => {
        return this.ossSrv.delOss(file.response.data.ossId).pipe(
            map(res => {
                return res.isComplete;
            })
        );
    };

    nzDownload = (file: NzUploadFile) => {
        this.fileSrv.oss(file.response.data.ossId);
    };

    nzPreview = (file: NzUploadFile) => {
        const _imgFile = {
            src: file.response.data.url,
            alt: file.name
        };
        this.nzImageService.preview([_imgFile], { nzZoom: 1.5, nzRotate: 0 });
    };
}
