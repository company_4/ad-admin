import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { FileSaveService } from '@basic';
import { deepCopy } from '@delon/util';
import { BaseOfSystemComponent } from '@routes/system/_base';
import { SystemOssService } from '@routes/system/oss/oss.service';
import { NzUploadChangeParam, NzUploadFile } from 'ng-zorro-antd/upload';
import { map } from 'rxjs';

@Component({
    selector: 'app-oss-upload-file',
    templateUrl: './upload-file.component.html',
    styleUrls: ['./upload-file.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OssUploadFileComponent extends BaseOfSystemComponent {
    actionUrl = this.apiUrl.resource.upload;

    // TODO: 文件上传的限制，后面都走配置项
    /**
     * 文件上传大小
     */
    nzSize: number = 5000;
    /**
     * 文件上传数量
     */
    nzLimit: number = 5;
    /**
     * 文件上传类型
     */
    nzAccept: string = '.doc,.xls,.ppt,.txt,.pdf';

    constructor(
        protected override injector: Injector,
        private ossSrv: SystemOssService,
        private fileSrv: FileSaveService
    ) {
        super(injector);
    }

    handleChange(info: NzUploadChangeParam): void {
        console.log(deepCopy(info));
        if (info.file.status !== 'uploading') {
            console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
            this.msg.success(`${info.file.name} 文件上传成功`);
        } else if (info.file.status === 'error') {
            this.msg.error(`${info.file.name} 文件上传失败`);
        }
    }

    nzRemove = (file: NzUploadFile) => {
        return this.ossSrv.delOss(file.response.data.ossId).pipe(
            map(res => {
                return res.isComplete;
            })
        );
    };

    nzDownload = (file: NzUploadFile) => {
        this.fileSrv.oss(file.response.data.ossId);
    };
}
