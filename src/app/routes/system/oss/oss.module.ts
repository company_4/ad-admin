import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SystemDictModule } from '@routes/system/dict/dict.module';
import { SharedModule } from '@shared';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzUploadModule } from 'ng-zorro-antd/upload';

import { SystemOssListComponent } from './oss-list/oss-list.component';
import { OssUploadFileComponent } from './upload-file/upload-file.component';
import { OssUploadImgComponent } from './upload-img/upload-img.component';

@NgModule({
    declarations: [SystemOssListComponent, OssUploadFileComponent, OssUploadImgComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index',
                component: SystemOssListComponent
            }
        ]),
        SystemDictModule,
        NzImageModule,
        NzToolTipModule,
        NzUploadModule
    ]
})
export class SystemOssModule {}
