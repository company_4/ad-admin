import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SystemDictModule } from '@routes/system/dict/dict.module';
import { SharedModule } from '@shared';

import { SystemConfigListComponent } from './config-list/config-list.component';
import { SystemEditConfigComponent } from './edit-config/edit-config.component';

@NgModule({
    declarations: [SystemConfigListComponent, SystemEditConfigComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index',
                component: SystemConfigListComponent
            }
        ]),
        SystemDictModule
    ]
})
export class SystemConfigModule {}
