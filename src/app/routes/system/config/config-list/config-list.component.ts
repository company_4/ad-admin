import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { STColumn } from '@delon/abc/st';
import { SFSchema } from '@delon/form';
import { UrlOfSystemTable } from '@routes/system/_base';
import { ConfigVO } from '@types';

import { SystemDictService } from '../../dict/dict.service';
import { SystemConfigService } from '../config.service';
import { SystemEditConfigComponent } from '../edit-config/edit-config.component';

@Component({
    selector: 'app-system-config-list',
    templateUrl: './config-list.component.html',
    styleUrls: ['./config-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemConfigListComponent extends UrlOfSystemTable implements OnInit {
    columns: STColumn[] = [
        {
            title: '编号',
            fixed: 'left',
            width: '50px',
            type: 'checkbox',
            className: 'text-center',
            exported: false
        },
        {
            title: '参数主键',
            index: 'configId',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.configId }) }
        },
        {
            title: '参数名称',
            index: 'configName',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.configName }) }
        },
        {
            title: '参数键名',
            index: 'configKey',
            className: 'text-center',
            width: '230px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.configKey }) }
        },
        {
            title: '参数键值',
            index: 'configValue',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.configValue }) }
        },
        {
            title: '系统内置',
            index: 'configType',
            className: 'text-center',
            width: '130px',
            render: 'configTypeCustom'
        },
        {
            title: '备注',
            index: 'remark',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.remark }) }
        },
        {
            title: '创建时间',
            index: 'createTime',
            className: 'text-center',
            width: '200px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.createTime }) }
        },
        {
            title: '操作',
            width: '230px',
            className: 'text-center',
            buttons: [
                {
                    text: '修改',
                    iif: item => item.roleId !== 1,
                    // tooltip: `修改`,
                    acl: ['admin', 'system:config:edit'],
                    icon: 'edit',
                    click: item => this.editConfig(item)
                },
                {
                    text: '删除',
                    iif: item => item.roleId !== 1,
                    // tooltip: '删除',
                    acl: ['admin', 'system:config:remove'],
                    icon: 'delete',
                    className: 'text-error',
                    click: item => this.delConfig(item)
                }
            ]
        }
    ];
    searchSchema: SFSchema = {
        properties: {
            configName: {
                type: 'string',
                title: '参数名称',
                ui: {
                    placeholder: '请输入参数名称'
                }
            },
            configKey: {
                type: 'string',
                title: '参数键名',
                ui: {
                    placeholder: '请输入参数键名'
                }
            },
            configType: {
                title: '系统内置',
                type: 'string',
                ui: {
                    placeholder: '系统内置',
                    widget: 'select',
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_yes_no')
                }
            },
            range: {
                type: 'string',
                title: '创建时间',
                ui: {
                    widget: 'date',
                    mode: 'range',
                    placeholder: '选择时间段'
                }
            }
        }
    };
    url: string = this.apiUrl.config.list;

    constructor(
        protected override injector: Injector,
        private config: SystemConfigService
    ) {
        super(injector);
    }

    addConfig() {
        this.modal
            .creatStaticModal(SystemEditConfigComponent, { item: {} }, 'md', {
                nzTitle: '添加参数'
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    editConfig(item: ConfigVO) {
        this.modal
            .creatStaticModal(SystemEditConfigComponent, { item, type: 'edit' }, 'md', {
                nzTitle: `修改参数【${item.configName}】`
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    delConfig(item: ConfigVO) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除参数编号为 ${item.configId} 的数据项？`,
            nzOnOk: () =>
                this.config.delConfig(item.configId.toString()).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }
    delConfigS(items: ConfigVO[]) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除参数编号为 ${items.map(xx => xx.configId).join(',')} 的数据项？`,
            nzOnOk: () =>
                this.config.delConfig(items.map(xx => xx.configId).join(',')).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }

    reloadCache() {
        this.config.refreshCache().subscribe(res => {
            if (res.isComplete) {
                this.msg.success('刷新成功');
            }
        });
    }
}
