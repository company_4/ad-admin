import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { ApiResToComponent } from '@basic';
import { SFSchema } from '@delon/form';
import { BaseOfSystemForm } from '@routes/system/_base';
import { ConfigForm, ConfigVO } from '@types';
import { Observable } from 'rxjs';

import { SystemDictService } from '../../dict/dict.service';
import { SystemConfigService } from '../config.service';

@Component({
    selector: 'app-system-edit-config',
    templateUrl: './edit-config.component.html',
    styleUrls: ['./edit-config.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemEditConfigComponent extends BaseOfSystemForm<ConfigForm, ConfigVO> implements OnInit {
    protected makeParams(value: any): ConfigForm {
        let _temp = this.getValuesFormSf(value);
        if (this.type === 'edit') {
            return { ..._temp, configId: this.item.configId };
        }
        return _temp;
    }

    schema: SFSchema = {
        properties: {
            configName: {
                type: 'string',
                title: '参数名称',
                ui: {
                    placeholder: '请输入参数名称',
                    grid: {
                        span: 24
                    }
                }
            },
            configKey: {
                type: 'string',
                title: '参数键名',
                ui: {
                    placeholder: '请输入参数键名',
                    grid: {
                        span: 24
                    }
                }
            },
            configValue: {
                type: 'string',
                title: '参数键值',
                ui: {
                    placeholder: '请输入参数键值',
                    grid: {
                        span: 24
                    }
                }
            },
            configType: {
                type: 'string',
                title: '系统内置',
                ui: {
                    widget: 'radio',
                    styleType: 'default',
                    buttonStyle: 'solid',
                    grid: {
                        span: 24
                    },
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_yes_no')
                },
                default: '0'
            },
            remark: {
                type: 'string',
                title: '备注',
                ui: {
                    widget: 'textarea',
                    placeholder: '请输入内容',
                    autosize: { minRows: 2, maxRows: 6 },
                    grid: {
                        span: 24
                    }
                }
            }
        },
        required: ['configName', 'configKey', 'configValue'],
        ui: {
            spanLabel: 6,
            spanControl: 16,
            grid: {
                span: 24
            }
        }
    };

    constructor(
        protected override injector: Injector,
        protected configSrv: SystemConfigService
    ) {
        super(injector);
    }

    override ngOnInit() {
        if (this.item.configId) {
            this.configSrv.getConfig(this.item.configId).subscribe(res => {
                this.item = res.data;
                this.cdRef.detectChanges();
            });
        }
    }

    sendToServer(value: any): Observable<ApiResToComponent<ConfigVO>> {
        if (this.item.configId) {
            return this.configSrv.updateConfig(value);
        }
        return this.configSrv.addConfig(value);
    }
}
