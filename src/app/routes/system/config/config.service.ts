import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SystemBasicService } from '@routes/system/_base';
import { ConfigForm, ConfigVO } from '@types';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SystemConfigService extends SystemBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    /**
     * 新增参数配置
     * @param params
     */
    addConfig(params: ConfigForm): Observable<ApiResToComponent<any>> {
        return this.client.post(this.apiUrl.config.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '新增参数配置', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 修改参数配置
     * @param params
     */
    updateConfig(params: ConfigForm): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.config.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '修改参数配置', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 查询参数详细
     */
    getConfig(configId: string | number): Observable<ApiResToComponent<ConfigVO>> {
        return this.client.get(this.apiUrl.config.getInfo + configId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询参数详细', color: 'grey' }, true, false);
            })
        );
    }
    /**
     * 根据参数键名查询参数值
     */
    getConfigKey(configKey: string): Observable<ApiResToComponent<string>> {
        return this.client.get(this.apiUrl.config.getConfigKey + configKey).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '根据参数键名查询参数值', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 删除参数配置
     */
    delConfig(configId: string | number): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.config.delConfig + configId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除参数配置', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 刷新参数缓存
     */
    refreshCache(): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.config.refreshCache).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '刷新参数缓存', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 修改参数配置
     * @param key
     * @param value
     */
    updateConfigByKey(key: string, value: boolean) {
        return this.client
            .put(this.apiUrl.config.updateConfigByKey, {
                configKey: key,
                configValue: value
            })
            .pipe(
                map((ev: ApiResponse) => {
                    return this.doWithResponse(ev, { msg: '修改参数配置', color: 'grey' }, true, false);
                })
            );
    }
}
