import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { ApiResToComponent } from '@basic';
import { SFSchema } from '@delon/form';
import { BaseOfSystemForm } from '@routes/system/_base';
import { DictTypeForm, DictTypeVO } from '@types';
import { Observable } from 'rxjs';

import { SystemDictService } from '../dict.service';

@Component({
    selector: 'app-system-edit-dict-type',
    templateUrl: './edit-dict-type.component.html',
    styleUrls: ['./edit-dict-type.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemEditDictTypeComponent extends BaseOfSystemForm<DictTypeForm, DictTypeVO> implements OnInit {
    protected makeParams(value: any): DictTypeForm {
        let _temp = this.getValuesFormSf(value);
        if (this.type === 'edit') {
            return { ..._temp, dictId: this.item.dictId };
        }
        return _temp;
    }

    schema: SFSchema = {
        properties: {
            dictName: {
                type: 'string',
                title: '字典名称',
                ui: {
                    placeholder: '请输入字典名称',
                    grid: {
                        span: 24
                    }
                }
            },
            dictType: {
                type: 'string',
                title: '字典类型',
                ui: {
                    placeholder: '请输入字典类型',
                    grid: {
                        span: 24
                    }
                }
            },
            status: {
                type: 'string',
                title: '状态',
                ui: {
                    widget: 'radio',
                    styleType: 'default',
                    buttonStyle: 'solid',
                    grid: {
                        span: 24
                    },
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_normal_disable')
                },
                default: '0'
            },
            remark: {
                type: 'string',
                title: '备注',
                ui: {
                    widget: 'textarea',
                    placeholder: '请输入内容',
                    autosize: { minRows: 2, maxRows: 6 },
                    grid: {
                        span: 24
                    }
                }
            }
        },
        required: ['dictName', 'dictType'],
        ui: {
            spanLabel: 6,
            spanControl: 16,
            grid: {
                span: 24
            }
        }
    };

    constructor(
        protected override injector: Injector,
        protected dictSrv: SystemDictService
    ) {
        super(injector);
    }

    override ngOnInit() {
        if (this.item.dictId) {
            this.dictSrv.getDictType(this.item.dictId).subscribe(res => {
                this.item = res.data;
                this.cdRef.detectChanges();
            });
        }
    }

    sendToServer(value: any): Observable<ApiResToComponent<DictTypeVO>> {
        if (this.item.dictId) {
            return this.dictSrv.updateDictType(value);
        }
        return this.dictSrv.addDictType(value);
    }
}
