import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { ApiResToComponent } from '@basic';
import { SFSchema } from '@delon/form';
import { BaseOfSystemForm } from '@routes/system/_base';
import { DictDataForm, DictDataVO } from '@types';
import { Observable } from 'rxjs';

import { SystemDictService } from '../dict.service';

@Component({
    selector: 'app-system-edit-dict-data',
    templateUrl: './edit-dict-data.component.html',
    styleUrls: ['./edit-dict-data.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemEditDictDataComponent extends BaseOfSystemForm<DictDataForm, DictDataVO> implements OnInit {
    protected makeParams(value: any): DictDataForm {
        let _temp = this.getValuesFormSf(value);
        if (this.type === 'edit') {
            return { ..._temp, dictCode: this.item.dictCode };
        }
        return _temp;
    }

    schema: SFSchema = {
        properties: {
            dictType: {
                type: 'string',
                title: '字典类型',
                ui: {
                    placeholder: '请输入字典类型',
                    grid: {
                        span: 24
                    }
                },
                readOnly: true
            },
            dictLabel: {
                type: 'string',
                title: '数据标签',
                ui: {
                    placeholder: '请输入数据标签',
                    grid: {
                        span: 24
                    }
                }
            },
            dictValue: {
                type: 'string',
                title: '数据键值',
                ui: {
                    placeholder: '请输入数据键值',
                    grid: {
                        span: 24
                    }
                }
            },
            cssClass: {
                type: 'string',
                title: '样式属性',
                ui: {
                    placeholder: '请输入样式属性',
                    grid: {
                        span: 24
                    }
                }
            },
            dictSort: {
                type: 'number',
                title: '显示排序',
                ui: {
                    grid: {
                        span: 24
                    }
                },
                default: 0
            },
            listClass: {
                type: 'string',
                title: '回显样式',
                ui: {
                    widget: 'select',
                    grid: {
                        span: 24
                    }
                },
                enum: [
                    { value: 'default', label: '默认' },
                    { value: 'processing', label: '主要' },
                    { value: 'success', label: '成功' },
                    { value: 'info', label: '信息' },
                    { value: 'warning', label: '警告' },
                    { value: 'error', label: '危险' }
                ]
            },
            status: {
                type: 'string',
                title: '状态',
                ui: {
                    widget: 'radio',
                    styleType: 'default',
                    buttonStyle: 'solid',
                    grid: {
                        span: 24
                    },
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_normal_disable')
                },
                default: '0'
            },
            remark: {
                type: 'string',
                title: '备注',
                ui: {
                    widget: 'textarea',
                    placeholder: '请输入内容',
                    autosize: { minRows: 2, maxRows: 6 },
                    grid: {
                        span: 24
                    }
                }
            }
        },
        required: ['dictLabel', 'dictValue', 'dictSort'],
        ui: {
            spanLabel: 6,
            spanControl: 16,
            grid: {
                span: 24
            }
        }
    };

    constructor(
        protected override injector: Injector,
        protected dictSrv: SystemDictService
    ) {
        super(injector);
    }

    override ngOnInit() {
        if (this.item.dictCode) {
            this.dictSrv.getDictData(this.item.dictCode).subscribe(res => {
                this.item = res.data;
                this.cdRef.detectChanges();
            });
        }
    }

    sendToServer(value: any): Observable<ApiResToComponent<DictDataVO>> {
        if (this.item.dictCode) {
            return this.dictSrv.updateDictData(value);
        }
        return this.dictSrv.addDictData(value);
    }
}
