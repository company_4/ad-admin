import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BasicDirectiveModule } from '@basic';
import { SharedModule } from '@shared';
import { NzTagModule } from 'ng-zorro-antd/tag';

import { SystemDictDataListComponent } from './dict-data-list/dict-data-list.component';
import { SystemDictTagComponent } from './dict-tag/dict-tag.component';
import { SystemDictTypeListComponent } from './dict-type-list/dict-type-list.component';
import { SystemDictPipe } from './dict.pipe';
import { SystemEditDictDataComponent } from './edit-dict-data/edit-dict-data.component';
import { SystemEditDictTypeComponent } from './edit-dict-type/edit-dict-type.component';

@NgModule({
    declarations: [
        SystemDictPipe,
        SystemDictTagComponent,
        SystemDictTypeListComponent,
        SystemEditDictTypeComponent,
        SystemDictDataListComponent,
        SystemEditDictDataComponent
    ],
    imports: [
        BasicDirectiveModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index2',
                component: SystemDictTypeListComponent
            }
        ]),
        NzTagModule
    ],
    exports: [SystemDictTagComponent, SystemDictPipe]
})
export class SystemDictModule {}
