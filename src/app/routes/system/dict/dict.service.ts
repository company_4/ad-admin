import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SystemBasicService } from '@routes/system/_base';
import { DictDataForm, DictDataOption, DictDataVO, DictTypeForm, DictTypeVO, ElTagType } from '@types';
import { map, Observable, of } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SystemDictService extends SystemBasicService {
    private _dictMap: Map<string, DictDataOption[]> = new Map();

    private _loading: boolean = false;

    constructor(protected override injector: Injector) {
        super(injector);
    }

    getDictsFromLocal(dictType: string) {
        return this._dictMap.get(dictType) ?? [];
    }

    /**
     * 获取字典数据
     * @param dictType
     */
    getDicts(dictType: string): Observable<DictDataOption[]> {
        if (!!this._dictMap.get(dictType)) {
            return of(this._dictMap.get(dictType) ?? []);
        }

        const fixClass = (clasz: string): ElTagType => {
            switch (clasz) {
                case 'primary':
                    return 'processing';
                    break;
                case 'danger':
                    return 'error';
                    break;
                case 'info':
                    return 'default';
                    break;
                default:
                    return clasz as ElTagType;
            }
            return clasz as ElTagType;
        };

        return this.client.get(`${this.apiUrl.dict.dictType}/${dictType}`).pipe(
            debounceTime(500),
            map((ev: ApiResponse) => {
                const res = ev.data.map(
                    (p: any): DictDataOption => ({
                        label: p.dictLabel,
                        value: p.dictValue,
                        elTagType: fixClass(p.listClass),
                        elTagClass: p.cssClass
                    })
                );
                this._dictMap = this._dictMap.set(dictType, res);
                return res;
            })
        );
    }

    /**
     * 新增字典类型
     * @param params
     */
    addDictType(params: DictTypeForm): Observable<ApiResToComponent<any>> {
        return this.client.post(this.apiUrl.dict.addOrUpdateType, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '新增字典类型', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 修改字典类型
     * @param params
     */
    updateDictType(params: DictTypeForm): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.dict.addOrUpdateType, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '修改字典类型', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 查询字典类型详细
     */
    getDictType(postId: string | number): Observable<ApiResToComponent<DictTypeVO>> {
        return this.client.get(this.apiUrl.dict.getTypeInfo + postId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询字典类型详细', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 删除字典类型
     */
    delDictType(dictId: string | number): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.dict.delDictType + dictId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除字典类型', color: 'grey' }, true, false);
            })
        );
    }
    /**
     * 刷新字典缓存
     */
    refreshCache(): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.dict.refreshCache).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '刷新字典缓存', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 新增字典数据
     * @param params
     */
    addDictData(params: DictDataForm): Observable<ApiResToComponent<any>> {
        return this.client.post(this.apiUrl.dict.addOrUpdateData, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '新增字典数据', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 修改字典数据
     * @param params
     */
    updateDictData(params: DictDataForm): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.dict.addOrUpdateData, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '修改字典数据', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 查询字典数据详细
     */
    getDictData(postId: string | number): Observable<ApiResToComponent<DictDataVO>> {
        return this.client.get(this.apiUrl.dict.getDataInfo + postId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询字典数据详细', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 删除字典数据
     */
    delDictData(dictCode: string): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.dict.delDictData + dictCode).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除字典数据', color: 'grey' }, true, false);
            })
        );
    }
}
