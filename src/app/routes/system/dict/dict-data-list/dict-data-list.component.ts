import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { STColumn } from '@delon/abc/st';
import { SFSchema } from '@delon/form';
import { UrlOfSystemTable } from '@routes/system/_base';
import { DictDataVO, DictTypeVO } from '@types';
import { NzModalRef } from 'ng-zorro-antd/modal';

import { SystemDictService } from '../dict.service';
import { SystemEditDictDataComponent } from '../edit-dict-data/edit-dict-data.component';

@Component({
    selector: 'app-system-dict-data-list',
    templateUrl: './dict-data-list.component.html',
    styleUrls: ['./dict-data-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemDictDataListComponent extends UrlOfSystemTable implements OnInit {
    @Input()
    item!: DictTypeVO;

    columns: STColumn[] = [
        {
            title: '编号',
            fixed: 'left',
            width: '50px',
            type: 'checkbox',
            className: 'text-center',
            exported: false
        },
        {
            title: '字典编码',
            index: 'dictCode',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.dictCode }) }
        },
        {
            title: '字典标签',
            index: 'dictLabel',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.dictLabel }) }
        },
        {
            title: '字典键值',
            index: 'dictValue',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.dictValue }) }
        },
        {
            title: '字典排序',
            index: 'dictSort',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.dictSort }) }
        },
        {
            title: '状态',
            index: 'status',
            className: 'text-center',
            width: '130px',
            render: 'statusCustom'
        },
        {
            title: '备注',
            index: 'remark',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.remark }) }
        },
        {
            title: '创建时间',
            index: 'createTime',
            className: 'text-center',
            width: '200px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.createTime }) }
        },
        {
            title: '操作',
            width: '150px',
            className: 'text-center',
            fixed: 'right',
            buttons: [
                {
                    text: '修改',
                    iif: item => item.roleId !== 1,
                    // tooltip: `修改`,
                    acl: ['admin', 'system:dict:edit'],
                    icon: 'edit',
                    click: item => this.editDictData(item)
                },
                {
                    text: '删除',
                    iif: item => item.roleId !== 1,
                    // tooltip: '删除',
                    acl: ['admin', 'system:dict:remove'],
                    icon: 'delete',
                    className: 'text-error',
                    click: item => this.delDictData(item)
                }
            ]
        }
    ];
    searchSchema: SFSchema = {
        properties: {
            dictLabel: {
                type: 'string',
                title: '字典标签',
                ui: {
                    placeholder: '请输入字典标签'
                }
            },
            status: {
                title: '状态',
                type: 'string',
                ui: {
                    placeholder: '字典状态',
                    widget: 'select',
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_normal_disable')
                }
            }
        }
    };
    url: string = this.apiUrl.dict.dataList;

    modalOpened: boolean = false;
    constructor(
        protected override injector: Injector,
        private dictSrv: SystemDictService,
        private modalRef: NzModalRef
    ) {
        super(injector);
    }

    override ngOnInit() {
        this.url = `${this.apiUrl.dict.dataList}?dictType=${this.item.dictType}`;
        this.modalRef.afterOpen.subscribe(res => {
            this.modalOpened = true;
            this.cdRef.detectChanges();
        });
    }

    addDictData() {
        this.modal
            .creatStaticModal(SystemEditDictDataComponent, { item: { dictType: this.item.dictType } }, 'md', {
                nzTitle: '添加字典数据'
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    editDictData(item: DictDataVO) {
        this.modal
            .creatStaticModal(SystemEditDictDataComponent, { item, type: 'edit' }, 'md', {
                nzTitle: `修改字典数据【${item.dictCode}】`
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    delDictData(item: DictDataVO) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除字典数据编号为 ${item.dictCode} 的数据项？`,
            nzOnOk: () =>
                this.dictSrv.delDictData(item.dictCode.toString()).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }
    delDictDataS(items: DictDataVO[]) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除字典数据编号为 ${items.map(xx => xx.dictCode).join(',')} 的数据项？`,
            nzOnOk: () =>
                this.dictSrv.delDictData(items.map(xx => xx.dictCode).join(',')).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }
}
