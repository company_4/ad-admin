import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Injector, Input, OnInit } from '@angular/core';
import { DictDataOption } from '@types';

import { SystemDictService } from '../dict.service';

@Component({
    selector: 'dict-tag',
    templateUrl: './dict-tag.component.html',
    styleUrls: ['./dict-tag.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemDictTagComponent implements OnInit {
    /**
     * 字典对应的值
     */
    @Input()
    value!: string | number;

    /**
     * 字典对应的key
     */
    @Input()
    dictKey!: string;

    dictSrv: SystemDictService;

    dictDataOption: DictDataOption[] = [];

    constructor(
        private injector: Injector,
        private cdr: ChangeDetectorRef
    ) {
        this.dictSrv = this.injector.get(SystemDictService);
    }

    ngOnInit() {
        if (this.dictKey) {
            this.dictSrv.getDicts(this.dictKey).subscribe(res => {
                this.dictDataOption = res;
                this.cdr.detectChanges();
            });
        }
    }
}
