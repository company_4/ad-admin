import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { STColumn } from '@delon/abc/st';
import { SFSchema } from '@delon/form';
import { UrlOfSystemTable } from '@routes/system/_base';
import { DictTypeVO } from '@types';

import { SystemDictDataListComponent } from '../dict-data-list/dict-data-list.component';
import { SystemDictService } from '../dict.service';
import { SystemEditDictTypeComponent } from '../edit-dict-type/edit-dict-type.component';

@Component({
    selector: 'app-system-dict-type-list',
    templateUrl: './dict-type-list.component.html',
    styleUrls: ['./dict-type-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemDictTypeListComponent extends UrlOfSystemTable implements OnInit {
    columns: STColumn[] = [
        {
            title: '编号',
            fixed: 'left',
            width: '50px',
            type: 'checkbox',
            className: 'text-center',
            exported: false
        },
        {
            title: '字典编号',
            index: 'dictId',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.dictId }) }
        },
        {
            title: '字典名称',
            index: 'dictName',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.dictName }) }
        },
        {
            title: '字典类型',
            index: 'dictType',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.dictType }) }
        },
        {
            title: '状态',
            index: 'status',
            className: 'text-center',
            width: '130px',
            render: 'statusCustom'
        },
        {
            title: '备注',
            index: 'remark',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.remark }) }
        },
        {
            title: '创建时间',
            index: 'createTime',
            className: 'text-center',
            width: '200px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.createTime }) }
        },
        {
            title: '操作',
            width: '230px',
            className: 'text-center',
            buttons: [
                {
                    text: '修改',
                    iif: item => item.roleId !== 1,
                    // tooltip: `修改`,
                    acl: ['admin', 'system:dict:edit'],
                    icon: 'edit',
                    click: item => this.editDictType(item)
                },
                {
                    text: '子项',
                    iif: item => item.roleId !== 1,
                    // tooltip: `修改`,
                    acl: ['admin', 'system:dict:edit'],
                    icon: 'unordered-list',
                    click: item => this.editDictTypeChildren(item)
                },
                {
                    text: '删除',
                    iif: item => item.roleId !== 1,
                    // tooltip: '删除',
                    acl: ['admin', 'system:dict:remove'],
                    icon: 'delete',
                    className: 'text-error',
                    click: item => this.delDictType(item)
                }
            ]
        }
    ];
    searchSchema: SFSchema = {
        properties: {
            dictName: {
                type: 'string',
                title: '字典名称',
                ui: {
                    placeholder: '请输入字典名称'
                }
            },
            dictType: {
                type: 'string',
                title: '字典类型',
                ui: {
                    placeholder: '请输入字典类型'
                }
            },
            status: {
                title: '状态',
                type: 'string',
                ui: {
                    placeholder: '字典状态',
                    widget: 'select',
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_normal_disable')
                }
            },
            range: {
                type: 'string',
                title: '创建时间',
                ui: {
                    widget: 'date',
                    mode: 'range',
                    placeholder: '选择时间段'
                }
            }
        }
    };
    url: string = this.apiUrl.dict.typeList;

    constructor(
        protected override injector: Injector,
        private dictSrv: SystemDictService
    ) {
        super(injector);
    }
    addDictType() {
        this.modal
            .creatStaticModal(SystemEditDictTypeComponent, { item: {} }, 'md', {
                nzTitle: '添加字典'
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    editDictType(item: DictTypeVO) {
        this.modal
            .creatStaticModal(SystemEditDictTypeComponent, { item, type: 'edit' }, 'md', {
                nzTitle: `修改字典【${item.dictName}】`
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    delDictType(item: DictTypeVO) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除字典编号为 ${item.dictId} 的数据项？`,
            nzOnOk: () =>
                this.dictSrv.delDictType(item.dictId.toString()).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }
    delDictTypeS(items: DictTypeVO[]) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除字典编号为 ${items.map(xx => xx.dictId).join(',')} 的数据项？`,
            nzOnOk: () =>
                this.dictSrv.delDictType(items.map(xx => xx.dictId).join(',')).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }

    reloadCache() {
        this.dictSrv.refreshCache().subscribe(res => {
            if (res.isComplete) {
                this.msg.success('刷新成功');
            }
        });
    }

    editDictTypeChildren(item: DictTypeVO) {
        this.modal
            .creatStaticModal(SystemDictDataListComponent, { item }, 'lg', {
                nzTitle: `字典【${item.dictName}】子项`
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
}
