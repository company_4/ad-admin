import { Injector, Pipe, PipeTransform } from '@angular/core';
import { SystemDictService } from '@routes/system/dict/dict.service';
import { DictDataOption } from '@types';

/**
 * {{ (item.status | dict: 'sys_normal_disable').label }}
 */
@Pipe({
    name: 'dict'
})
export class SystemDictPipe implements PipeTransform {
    dictSrv: SystemDictService;
    constructor(private injector: Injector) {
        this.dictSrv = this.injector.get(SystemDictService);
    }
    /**
     * 按长度截取字符串
     * @param str string
     * @param key string
     */
    transform(str: string | number, key: string): string {
        const res = this.dictSrv.getDictsFromLocal(key);
        return res[str as any].label;
    }
}
