import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { ApiResToComponent } from '@basic';
import { SFSchema } from '@delon/form';
import { BaseOfSystemForm } from '@routes/system/_base';
import { SystemDictService } from '@routes/system/dict/dict.service';
import { OssConfigForm, OssConfigVO } from '@types';
import { Observable } from 'rxjs';

import { SystemOssConfigService } from '../oss-config.service';

@Component({
    selector: 'app-system-oss-edit-config',
    templateUrl: './edit-config.component.html',
    styleUrls: ['./edit-config.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemOssEditConfigComponent extends BaseOfSystemForm<OssConfigForm, OssConfigVO> implements OnInit {
    protected makeParams(value: any): OssConfigForm {
        let _temp = this.getValuesFormSf(value);
        if (this.type === 'edit') {
            return { ..._temp, ossConfigId: this.item.ossConfigId };
        }
        return _temp;
    }

    schema: SFSchema = {
        properties: {
            configKey: {
                type: 'string',
                title: '配置key',
                ui: {
                    placeholder: '请输入配置key',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    }
                }
            },
            endpoint: {
                type: 'string',
                title: '访问站点',
                ui: {
                    placeholder: '请输入访问站点',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    }
                },
                minLength: 2,
                maxLength: 200
            },
            domain: {
                type: 'string',
                title: '自定义域名',
                ui: {
                    placeholder: '请输入自定义域名',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    },
                    optionalHelp: '请勿输入https或http，请在是否https里选择'
                }
            },
            accessKey: {
                type: 'string',
                title: 'accessKey',
                ui: {
                    placeholder: '请输入accessKey',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    }
                },
                minLength: 2,
                maxLength: 200
            },
            secretKey: {
                type: 'string',
                title: 'secretKey',
                ui: {
                    placeholder: '请输入秘钥',
                    widget: 'password',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    }
                },
                minLength: 2,
                maxLength: 200
            },
            bucketName: {
                type: 'string',
                title: '桶名称',
                ui: {
                    placeholder: '请输入桶名称',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    }
                },
                minLength: 2,
                maxLength: 200
            },
            prefix: {
                type: 'string',
                title: '前缀',
                ui: {
                    placeholder: '请输入前缀',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    }
                }
            },
            isHttps: {
                type: 'string',
                title: '是否HTTPS',
                ui: {
                    widget: 'radio',
                    styleType: 'default',
                    buttonStyle: 'solid',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    },
                    asyncData: () => this.injector.get(SystemDictService).getDicts('sys_yes_no')
                }
            },
            accessPolicy: {
                type: 'string',
                title: '桶权限类型',
                ui: {
                    widget: 'radio',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    }
                },
                enum: [
                    { label: 'private', value: '0' },
                    { label: 'public', value: '1' },
                    { label: 'custom', value: '2' }
                ]
            },
            region: {
                type: 'string',
                title: '域',
                ui: {
                    placeholder: '请输入域',
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    }
                }
            },
            remark: {
                type: 'string',
                title: '备注',
                ui: {
                    widget: 'textarea',
                    placeholder: '请输入内容',
                    autosize: { minRows: 2, maxRows: 6 },
                    spanLabel: 4,
                    spanControl: 20,
                    grid: {
                        span: 24
                    }
                }
            }
        },
        required: ['configKey', 'accessKey', 'secretKey', 'bucketName', 'endpoint', 'accessPolicy'],
        ui: {
            spanLabel: 8,
            spanControl: 16,
            grid: {
                span: 24
            }
        }
    };

    constructor(
        protected override injector: Injector,
        protected ossConfigSrv: SystemOssConfigService
    ) {
        super(injector);
    }

    override ngOnInit() {
        if (this.item.ossConfigId) {
            this.ossConfigSrv.getOssConfig(this.item.ossConfigId).subscribe(res => {
                this.item = res.data;
                this.cdRef.detectChanges();
            });
        }
    }

    sendToServer(value: any): Observable<ApiResToComponent<OssConfigVO>> {
        if (this.item.ossConfigId) {
            return this.ossConfigSrv.updateOssConfig(value);
        }
        return this.ossConfigSrv.addOssConfig(value);
    }
}
