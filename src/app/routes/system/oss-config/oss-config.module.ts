import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SystemDictModule } from '@routes/system/dict/dict.module';
import { SharedModule } from '@shared';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

import { SystemOssConfigListComponent } from './config-list/config-list.component';
import { SystemOssEditConfigComponent } from './edit-config/edit-config.component';

@NgModule({
    declarations: [SystemOssConfigListComponent, SystemOssEditConfigComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: 'index',
                component: SystemOssConfigListComponent
            }
        ]),
        NzImageModule,
        NzToolTipModule,
        SystemDictModule,
        NzTagModule,
        NzPopconfirmModule
    ]
})
export class SystemOssConfigModule {}
