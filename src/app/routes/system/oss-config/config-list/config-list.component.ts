import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { STColumn } from '@delon/abc/st';
import { SFSchema } from '@delon/form';
import { UrlOfSystemTable } from '@routes/system/_base';

import { SystemOssEditConfigComponent } from '../edit-config/edit-config.component';
import { SystemOssConfigService } from '../oss-config.service';

@Component({
    selector: 'app-system-oss-config-list',
    templateUrl: './config-list.component.html',
    styleUrls: ['./config-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemOssConfigListComponent extends UrlOfSystemTable implements OnInit {
    columns: STColumn[] = [
        {
            title: '编号',
            fixed: 'left',
            width: '50px',
            type: 'checkbox',
            className: 'text-center',
            exported: false
        },
        {
            title: '主建',
            index: 'ossConfigId',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.ossConfigId }) }
        },
        {
            title: '配置key',
            index: 'configKey',
            className: 'text-center',
            width: '130px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.configKey }) }
        },
        {
            title: '访问站点',
            index: 'endpoint',
            className: 'text-center',
            width: '230px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.endpoint }) }
        },
        {
            title: '自定义域名',
            index: 'domain',
            className: 'text-center',
            width: '230px',
            type: 'widget',
            widget: { type: 'long-text', params: ({ record }) => ({ text: record.domain }) }
        },
        {
            title: '桶名称',
            index: 'bucketName',
            className: 'text-center',
            width: '130px'
        },
        {
            title: '前缀',
            index: 'prefix',
            className: 'text-center',
            width: '130px'
        },
        {
            title: '域',
            index: 'region',
            className: 'text-center',
            width: '130px'
        },
        {
            title: '桶权限类型',
            index: 'accessPolicy',
            className: 'text-center',
            width: '130px',
            render: 'accessPolicyCustom'
        },
        {
            title: '是否默认',
            index: 'status',
            className: 'text-center',
            width: '130px',
            render: 'statusCustom'
        },
        {
            title: '操作',
            width: '230px',
            className: 'text-center',
            buttons: [
                {
                    text: '修改',
                    // tooltip: `修改`,
                    acl: ['admin', 'system:oss:edit'],
                    icon: 'edit',
                    click: item => this.editConfig(item)
                },
                {
                    text: '删除',
                    // tooltip: '删除',
                    acl: ['admin', 'system:oss:remove'],
                    icon: 'delete',
                    className: 'text-error',
                    click: item => this.delConfig(item)
                }
            ]
        }
    ];
    searchSchema: SFSchema = {
        properties: {
            configKey: {
                type: 'string',
                title: '配置key',
                ui: {
                    placeholder: '请输入配置key'
                }
            },
            bucketName: {
                type: 'string',
                title: '桶名称',
                ui: {
                    placeholder: '请输入桶名称'
                }
            },
            status: {
                title: '是否默认',
                type: 'string',
                ui: {
                    placeholder: '是否默认',
                    widget: 'select'
                },
                enum: [
                    { label: '是', value: '0' },
                    { label: '否', value: '1' }
                ]
            }
        }
    };
    url: string = this.apiUrl.resource.configList;

    constructor(
        protected override injector: Injector,
        protected ossConfigSrv: SystemOssConfigService
    ) {
        super(injector);
    }

    addConfig() {
        this.modal
            .creatStaticModal(SystemOssEditConfigComponent, { item: {} }, 'lg', {
                nzTitle: '添加对象存储配置'
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    editConfig(item: any) {
        this.modal
            .creatStaticModal(SystemOssEditConfigComponent, { item, type: 'edit' }, 'lg', {
                nzTitle: `修改对象存储配置`
            })
            .subscribe(res => {
                if (res) {
                    this.st.reset();
                }
            });
    }
    delConfigS(items: any[]) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除OSS配置编号为 ${items.map(xx => xx.ossConfigId).join(',')} 的数据项？`,
            nzOnOk: () =>
                this.ossConfigSrv.delOssConfig(items.map(xx => xx.ossConfigId).join(',')).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }
    delConfig(item: any) {
        this.modalSrv.confirm({
            nzTitle: '删除提示',
            nzContent: `是否确认删除OSS配置编号为 ${item.ossConfigId} 的数据项？`,
            nzOnOk: () =>
                this.ossConfigSrv.delOssConfig(item.ossConfigId.toString()).subscribe(res => {
                    if (res.isComplete) {
                        this.msg.success('删除成功');
                    }
                    this.st.reload();
                })
        });
    }

    /**
     * 获取提示信息
     * @param row
     */
    getPopConfirmText(row: any) {
        let text = row.status === '0' ? '停用' : '启用';
        return `确认要"${text}""${row.configKey}"配置吗?`;
    }

    /**
     * 更改状态取消
     * @param item
     * @param index
     */
    cancelStatus(item: any, index: number) {
        this.st.setRow(index, { status: item.status }, { refreshSchema: true });
    }

    /**
     * 更改状态确认
     * @param item
     * @param index
     */
    confirmStatus(item: any, index: number) {
        const text = item.status === '0' ? '停用' : '启用';
        const _status = item.status === '0' ? '1' : '0';
        this.ossConfigSrv
            .changeOssConfigStatus({ ossConfigId: item.ossConfigId, status: _status, configKey: item.configKey })
            .subscribe(res => {
                this.msg.success(`${text}成功`);
                this.st.reload();
            });
    }
}
