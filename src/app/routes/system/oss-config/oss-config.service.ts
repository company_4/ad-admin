import { Injectable, Injector } from '@angular/core';
import { ApiResponse, ApiResToComponent } from '@basic';
import { SystemBasicService } from '@routes/system/_base';
import { NoticeVO, OssConfigForm } from '@types';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SystemOssConfigService extends SystemBasicService {
    constructor(protected override injector: Injector) {
        super(injector);
    }

    /**
     * 新增对象存储配置
     * @param params
     */
    addOssConfig(params: OssConfigForm): Observable<ApiResToComponent<any>> {
        return this.client.post(this.apiUrl.resource.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '新增对象存储配置', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 修改对象存储配置
     * @param params
     */
    updateOssConfig(params: OssConfigForm): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.resource.addOrUpdate, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '修改对象存储配置', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 查询对象存储配置详细
     */
    getOssConfig(ossConfigId: string | number): Observable<ApiResToComponent<NoticeVO>> {
        return this.client.get(this.apiUrl.resource.getOssConfig + ossConfigId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '查询对象存储配置详细', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 删除对象存储配置
     */
    delOssConfig(ossConfigId: string | number): Observable<ApiResToComponent<null>> {
        return this.client.delete(this.apiUrl.resource.delOssConfig + ossConfigId).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '删除对象存储配置', color: 'grey' }, true, false);
            })
        );
    }

    /**
     * 对象存储状态修改
     * @param params
     */
    changeOssConfigStatus(params: { ossConfigId: string | number; status: string; configKey: string }): Observable<ApiResToComponent<any>> {
        return this.client.put(this.apiUrl.resource.changeOssConfigStatus, params).pipe(
            map((ev: ApiResponse) => {
                return this.doWithResponse(ev, { msg: '对象存储状态修改', color: 'grey' }, true, false);
            })
        );
    }
}
