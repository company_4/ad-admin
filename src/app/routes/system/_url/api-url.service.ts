import { Injectable } from '@angular/core';
import { AbstractApiUrlDataService } from '@basic/abstracts';

@Injectable({ providedIn: 'any' })
export class ApiUrlDataOfSystemService extends AbstractApiUrlDataService {
    user = {
        // 获取用户列表
        list: `${this.baseUrl}/system/user/list`,
        // 新增或更新
        addOrUpdate: `${this.baseUrl}/system/user`,
        // 查询用户详细
        getInfo: `${this.baseUrl}/system/user/`,
        // 删除用户
        delUsers: `${this.baseUrl}/system/user/`,
        // 用户状态修改
        changeStatus: `${this.baseUrl}/system/user/changeStatus`,
        // 用户密码重置
        resetPwd: `${this.baseUrl}/system/user/resetPwd`,
        // 查询部门下拉树结构
        deptTree: `${this.baseUrl}/system/user/deptTree`
    };

    dict = {
        dictType: `${this.baseUrl}/system/dict/data/type`,
        // 查询字典列表
        typeList: `${this.baseUrl}/system/dict/type/list`,
        // 查询字典详细
        getTypeInfo: `${this.baseUrl}/system/dict/type/`,
        // 新增或更新
        addOrUpdateType: `${this.baseUrl}/system/dict/type`,
        // 删除字典
        delDictType: `${this.baseUrl}/system/dict/type/`,
        // 刷新字典缓存
        refreshCache: `${this.baseUrl}/system/dict/type/refreshCache`,
        // 查询字典数据列表
        dataList: `${this.baseUrl}/system/dict/data/list`,
        // 查询字典数据详细
        getDataInfo: `${this.baseUrl}/system/dict/data/`,
        // 新增或更新
        addOrUpdateData: `${this.baseUrl}/system/dict/data`,
        // 删除字典数据
        delDictData: `${this.baseUrl}/system/dict/data/`
    };

    post = {
        // 查询岗位列表
        list: `${this.baseUrl}/system/post/list`,
        // 查询岗位详细
        getInfo: `${this.baseUrl}/system/post/`,
        // 新增或更新
        addOrUpdate: `${this.baseUrl}/system/post`,
        // 删除岗位
        delPost: `${this.baseUrl}/system/post/`
    };

    notice = {
        // 查询公告列表
        list: `${this.baseUrl}/system/notice/list`,
        // 查询公告详细
        getInfo: `${this.baseUrl}/system/notice/`,
        // 新增或更新
        addOrUpdate: `${this.baseUrl}/system/notice`,
        // 删除公告
        delNotice: `${this.baseUrl}/system/notice/`
    };

    config = {
        // 查询岗位列表
        list: `${this.baseUrl}/system/config/list`,
        // 查询岗位详细
        getInfo: `${this.baseUrl}/system/config/`,
        // 新增或更新
        addOrUpdate: `${this.baseUrl}/system/config`,
        // 根据参数键名查询参数值
        getConfigKey: `${this.baseUrl}/system/config/configKey/`,
        // 修改参数配置
        updateConfigByKey: `${this.baseUrl}/system/config/updateByKey`,
        // 刷新参数缓存
        refreshCache: `${this.baseUrl}/system/config/refreshCache`,
        // 删除参数配置
        delConfig: `${this.baseUrl}/system/config/`
    };

    dept = {
        list: `${this.baseUrl}/system/dept/list`,
        // 查询部门详细
        getInfo: `${this.baseUrl}/system/dept/`,
        // 新增或更新
        addOrUpdate: `${this.baseUrl}/system/dept`,
        // 删除部门
        delDept: `${this.baseUrl}/system/dept/`
    };

    menu = {
        // 查询菜单下拉树结构
        treeSelect: `${this.baseUrl}/system/menu/treeselect`,
        // 查询菜单列表
        list: `${this.baseUrl}/system/menu/list`,
        // 根据角色ID查询菜单下拉树结构
        roleMenuTreeselect: `${this.baseUrl}/system/menu/roleMenuTreeselect/`,
        // 查询菜单详细
        getInfo: `${this.baseUrl}/system/menu/`,
        // 新增或更新
        addOrUpdate: `${this.baseUrl}/system/menu`,
        // 删除菜单
        delMenu: `${this.baseUrl}/system/menu/`
    };

    role = {
        // 查询角色列表
        list: `${this.baseUrl}/system/role/list`,
        // 角色状态修改
        changeStatus: `${this.baseUrl}/system/role/changeStatus`,
        // 新增或更新
        addOrUpdate: `${this.baseUrl}/system/role`,
        // 查询角色详细
        getInfo: `${this.baseUrl}/system/role/`,
        // 删除角色
        delRoles: `${this.baseUrl}/system/role/`,
        // 根据角色ID查询部门树结构
        deptTreeSelect: `${this.baseUrl}/system/role/deptTree/`,
        // 角色数据权限
        dataScope: `${this.baseUrl}/system/role/dataScope`,
        // 查询角色已授权用户列表
        allocatedList: `${this.baseUrl}/system/role/authUser/allocatedList`,
        // 查询角色未授权用户列表
        unallocatedList: `${this.baseUrl}/system/role/authUser/unallocatedList`,
        // 授权用户选择
        selectAll: `${this.baseUrl}/system/role/authUser/selectAll`,
        // 取消用户授权角色
        cancelUser: `${this.baseUrl}/system/role/authUser/cancel`,
        // 批量取消用户授权角色
        authUserCancelAll: `${this.baseUrl}/system/role/authUser/cancelAll`
    };

    loginInfo = {
        // 查询登录日志列表
        list: `${this.baseUrl}/system/logininfor/list`,
        // 删除登录日志
        delLoginInfo: `${this.baseUrl}/system/logininfor/`,
        // 删除登录日志
        unlockLoginInfo: `${this.baseUrl}/system/logininfor/unlock/`,
        // 清空登录日志
        cleanLoginInfo: `${this.baseUrl}/system/logininfor/clean`
    };
    operateLog = {
        // 查询操作日志列表
        list: `${this.baseUrl}/system/operlog/list`,
        // 删除操作日志
        delOperateLog: `${this.baseUrl}/system/operlog/`,
        // 清空操作日志
        cleanOperateLog: `${this.baseUrl}/system/operlog/clean`
    };

    resource = {
        // 查询操作日志列表
        list: `${this.baseUrl}/resource/oss/list`,
        // 查询OSS对象基于id串
        listByIds: `${this.baseUrl}/resource/oss/listByIds/`,
        // 删除OSS对象存储
        delOss: `${this.baseUrl}/resource/oss/`,
        // 文件上传
        upload: `${this.baseUrl}/resource/oss/upload`,
        // 查询对象存储配置列表
        configList: `${this.baseUrl}/resource/oss/config/list`,
        // 查询对象存储配置详细
        getOssConfig: `${this.baseUrl}/resource/oss/config/`,
        // 新增或更新
        addOrUpdate: `${this.baseUrl}/resource/oss/config`,
        // 删除对象存储配置
        delOssConfig: `${this.baseUrl}/resource/oss/config/`,
        // 对象存储状态修改
        changeOssConfigStatus: `${this.baseUrl}/resource/oss/config/changeStatus`
    };
}
