import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SystemOssModule } from '@routes/system/oss/oss.module';
import { SystemOssConfigModule } from '@routes/system/oss-config/oss-config.module';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forChild([
            { path: 'user', loadChildren: () => import('./user/user.module').then(m => m.SystemUserModule) },
            { path: 'role', loadChildren: () => import('./role/role.module').then(m => m.SystemRoleModule) },
            { path: 'menu', loadChildren: () => import('./menu/menu.module').then(m => m.SystemMenuModule) },
            { path: 'dept', loadChildren: () => import('./dept/dept.module').then(m => m.SystemDeptModule) },
            { path: 'post', loadChildren: () => import('./post/post.module').then(m => m.SystemPostModule) },
            { path: 'dict', loadChildren: () => import('./dict/dict.module').then(m => m.SystemDictModule) },
            { path: 'config', loadChildren: () => import('./config/config.module').then(m => m.SystemConfigModule) },
            { path: 'notice', loadChildren: () => import('./notice/notice.module').then(m => m.SystemNoticeModule) },
            { path: 'oss', loadChildren: () => import('./oss/oss.module').then(m => m.SystemOssModule) },
            { path: 'oss-config', loadChildren: () => import('./oss-config/oss-config.module').then(m => m.SystemOssConfigModule) },
            { path: 'logininfor', loadChildren: () => import('./log/login-info/login-info.module').then(m => m.SystemLoginInfoModule) },
            { path: 'operlog', loadChildren: () => import('./log/operate-log/operate-log.module').then(m => m.SystemOperateLogModule) }
        ])
    ]
})
export class SystemModule {}
