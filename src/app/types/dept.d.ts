import { BaseEntity, PageQuery } from './global';

/**
 * 菜单树形结构类型
 */
export interface DeptTreeOption {
    id: string;
    label: string;
    parentId: string;
    weight: number;
    children?: DeptTreeOption[];
}

/**
 * 部门查询参数
 */
export interface DeptQuery extends PageQuery {
    deptName?: string;
    status?: number;
}

/**
 * 部门类型
 */
export interface DeptVO extends BaseEntity {
    id: number | string;
    parentName: string;
    parentId: number | string;
    children: DeptVO[];
    deptId: number | string;
    deptName: string;
    orderNum: number;
    leader: string;
    phone: string;
    email: string;
    status: string;
    delFlag: string;
    ancestors: string;
    menuId: string | number;
}

/**
 * 部门表单类型
 */
export interface DeptForm {
    parentName?: string;
    parentId?: number | string;
    children?: DeptForm[];
    deptId?: number | string;
    deptName?: string;
    orderNum?: number;
    leader?: string;
    phone?: string;
    email?: string;
    status?: string;
    delFlag?: string;
    ancestors?: string;
}
