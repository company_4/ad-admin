import { BaseEntity, PageQuery } from './global';

export interface OssVO extends BaseEntity {
    ossId: string | number;
    fileName: string;
    originalName: string;
    fileSuffix: string;
    url: string;
    createByName: string;
    service: string;
}

export interface OssQuery extends PageQuery {
    fileName: string;
    originalName: string;
    fileSuffix: string;
    createTime: string;
    service: string;
    orderByColumn: string;
    isAsc: string;
}
export interface OssForm {
    file: undefined | string;
}

export interface OssConfigVO extends BaseEntity {
    ossConfigId: number | string;
    configKey: string;
    accessKey: string;
    secretKey: string;
    bucketName: string;
    prefix: string;
    endpoint: string;
    domain: string;
    isHttps: string;
    region: string;
    status: string;
    ext1: string;
    remark: string;
    accessPolicy: string;
}

export interface OssConfigQuery extends PageQuery {
    configKey: string;
    bucketName: string;
    status: string;
}

export interface OssConfigForm {
    ossConfigId: string | number | undefined;
    configKey: string;
    accessKey: string;
    secretKey: string;
    bucketName: string;
    prefix: string;
    endpoint: string;
    domain: string;
    isHttps: string;
    accessPolicy: string;
    region: string;
    status: string;
    remark: string;
}
