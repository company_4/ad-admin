import { BaseEntity, PageQuery } from './global';
import { DeptTreeOption } from './dept';

export interface RoleDeptTree {
    checkedKeys: string[];
    depts: DeptTreeOption[];
}

export interface RoleVO extends BaseEntity {
    roleId: string | number;
    roleName: string;
    roleKey: string;
    roleSort: number;
    dataScope: string;
    menuCheckStrictly: boolean;
    deptCheckStrictly: boolean;
    status: string;
    delFlag: string;
    remark?: any;
    flag: boolean;
    menuIds?: Array<string | number>;
    deptIds?: Array<string | number>;
    admin: boolean;
}

export interface RoleQuery extends PageQuery {
    roleName: string;
    roleKey: string;
    status: string;
}

export interface RoleForm {
    roleName: string;
    roleKey: string;
    roleSort: number;
    status: string;
    menuCheckStrictly: boolean;
    deptCheckStrictly: boolean;
    remark: string;
    dataScope?: string;
    roleId: string | undefined;
    menuIds: Array<string | number>;
    deptIds: Array<string | number>;
}
