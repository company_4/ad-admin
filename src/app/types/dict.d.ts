import { BaseEntity, ElTagType, PageQuery } from './global';
export interface DictDataQuery extends PageQuery {
    dictName: string;
    dictType: string;
    dictLabel: string;
}

export interface DictDataVO extends BaseEntity {
    dictCode: string;
    dictType: string;
    dictLabel: string;
    dictValue: string;
    cssClass: string;
    listClass: ElTagType;
    dictSort: number;
    remark: string;
}

export interface DictDataForm {
    dictType?: string;
    dictCode: string | undefined;
    dictLabel: string;
    dictValue: string;
    cssClass: string;
    listClass: ElTagType;
    dictSort: number;
    remark: string;
}

export interface DictTypeVO extends BaseEntity {
    dictId: number | string;
    dictName: string;
    dictType: string;
    remark: string;
}

export interface DictTypeForm {
    dictId: number | string | undefined;
    dictName: string;
    dictType: string;
    remark: string;
}

export interface DictTypeQuery extends PageQuery {
    dictName: string;
    dictType: string;
}
